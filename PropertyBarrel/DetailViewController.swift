//
//  DetailViewController.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/11/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    var app = UIApplication.shared.delegate as! AppDelegate
    var viewexpand:UIView!
    var viewbackground:UIView!
    var viewshare:UIView!
    var popview:UIView!
    
    var btnback: UIButton!
    var btncall: UIButton!
    var btnsms:  UIButton!
    var btnshow: UIButton!
    var btnfevdark : UIButton!
    var btnmap:  UIButton!
    var btndirection: UIButton!
    var btnshare : UIButton!
    var btnleft : UIButton!
    var btnright : UIButton!
    var btnmenu:UIButton!
    
    var imgprofile:UIImageView!
    var imgback : UIImageView!
    var middleview : UIView!
    var bottomview : UIView!
    var textview : UITextView!
    
    var lbl_category_name:UILabel!
    
    var myDatePicker:UIDatePicker!
    
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selectedlist:Bool!
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]
    var selected:Bool!
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    override func viewDidLoad() {
        
        self.view.backgroundColor = UIColor.init(red:255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        
        super.viewDidLoad()
        selected=false
        loadinitialUI()
    }
    
    func loadinitialUI()
    {
        let topview = UIView(frame:CGRect(x: 0, y: 0, width: app.screenWidth, height:30))
        topview.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        self.view.addSubview(topview)
        
       let imgback = UIButton(frame: CGRect(x:5, y:5, width:20, height:20))
        imgback.setBackgroundImage(UIImage(named:"ic_det_back"), for: .normal)
        //imgback.image = UIImage(named: "ic_det_back")
        imgback.alpha=0.6
        imgback.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(imgback)
        
        btnback = UIButton(frame:CGRect(x:imgback.frame.maxX, y:5, width:50, height:20))
        btnback.setTitleColor(UIColor.init(red: 202/255.0, green: 202/255.0, blue: 202/255.0, alpha: 1.0), for: UIControlState.normal)
        btnback.setTitle("Back", for: UIControlState.normal)
        btnback.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnback.titleLabel?.font=UIFont.systemFont(ofSize: 18)
        topview.addSubview(btnback)
        
        btnshow = UIButton(frame:CGRect(x:app.screenWidth-120, y:5, width:20, height:18))
        btnshow.setImage( UIImage(named: "ic_dettop_showing1"), for: UIControlState.normal)
        btnshow.titleLabel?.font=UIFont.systemFont(ofSize: 20)
        btnshow.alpha=0.6
        btnshow.addTarget(self, action:#selector(btnshowclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(btnshow)
        
        btnfevdark = UIButton(frame:CGRect(x:app.screenWidth-90, y:5, width:20, height:18))
        btnfevdark.setImage( UIImage(named: "ic_dettop_fev1"), for: UIControlState.normal)
        btnfevdark.titleLabel?.font=UIFont.systemFont(ofSize: 20)
        btnfevdark.alpha=0.6
        topview.addSubview(btnfevdark)
        
        btnsms = UIButton(frame:CGRect(x:app.screenWidth-60, y:5, width:20, height:20))
        btnsms.setImage( UIImage(named: "ic_dettop_msg1"), for: UIControlState.normal)
        btnsms.titleLabel?.font=UIFont.systemFont(ofSize: 18)
        btnsms.alpha=0.6
        topview.addSubview(btnsms)
        
        btncall = UIButton(frame:CGRect(x:app.screenWidth-30, y:5, width:20, height:20))
        btncall.setImage( UIImage(named: "ic_dettop_phone"), for: UIControlState.normal)
        btncall.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btncall.alpha=0.6
        topview.addSubview(btncall)
        
        imgprofile = UIImageView(frame: CGRect(x: 0, y:topview.frame.maxY, width: app.screenWidth, height:app.screenHeight/2-100))
        imgprofile.image = UIImage(named: "home")
        self.view.addSubview(imgprofile)
        
        let lblpic1=UIView(frame: CGRect(x:imgprofile.frame.size.width-60, y:imgprofile.frame.size.height-30, width:50, height:25))
        lblpic1.backgroundColor=UIColor.init(red: 235/255.0, green: 235/255.0, blue: 229/255.0, alpha: 1.0)
        lblpic1.layer.cornerRadius=5
        imgprofile.addSubview(lblpic1)
        let lblpic :UILabel=UILabel(frame: CGRect(x:0, y:0, width:50, height:25))
        lblpic.layer.cornerRadius = 8.0
        lblpic.text = "1/17"
        lblpic.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        //lblpic.backgroundColor = UIColor.init(red: 235/255.0, green: 235/255.0, blue: 229/255.0, alpha: 1.0)
        lblpic.font = UIFont.boldSystemFont(ofSize: 10)
        lblpic.textAlignment = .center
        lblpic.layer.cornerRadius=5
        lblpic1.addSubview(lblpic)
        
        btnleft = UIButton(frame:CGRect(x:0, y:7, width:10, height:10))
        btnleft.setImage( UIImage(named: "left-arrow"), for: UIControlState.normal)
        btnleft.alpha=0.6
        lblpic1.addSubview(btnleft)
        
        btnright = UIButton(frame:CGRect(x:lblpic.frame.size.width-10, y:7, width:10, height:10))
        btnright.setImage( UIImage(named: "play-arrow"), for: UIControlState.normal)
        btnright.alpha=0.6
        lblpic1.addSubview(btnright)
        
        let  lblname :UILabel=UILabel(frame: CGRect(x: 10, y: imgprofile.frame.maxY, width:55, height:30))
        lblname.text = "Status:"
        lblname.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblname.font = UIFont.boldSystemFont(ofSize: 15)
        lblname.textAlignment = .left
        self.view.addSubview(lblname)
        
        let  lblactive :UILabel=UILabel(frame: CGRect(x:lblname.frame.maxX , y: imgprofile.frame.maxY, width: 200, height:30))
        lblactive.text = "Active"
        lblactive.textColor = UIColor.init(red:106/255.0, green: 139/255.0, blue: 160/255.0, alpha: 1.0)
        lblactive.font = UIFont.boldSystemFont(ofSize: 15)
        lblactive.textAlignment = .left
        self.view.addSubview(lblactive)
        
        let  lbledit :UILabel=UILabel(frame: CGRect(x:app.screenWidth-130, y: imgprofile.frame.maxY+3, width:125, height: 30))
        lbledit.text = "$734,234"
        lbledit.textColor = UIColor.init(red:106/255.0, green: 139/255.0, blue: 160/255.0, alpha: 1.0)
        lbledit.font = UIFont.boldSystemFont(ofSize:20)
        lbledit.textAlignment = .center
        self.view.addSubview(lbledit)
        
         let  lbladress :UILabel=UILabel(frame: CGRect(x:10, y: lblname.frame.maxY+5, width:app.screenWidth, height:20))
        lbladress.text = "123 Fairview Park Trail,Calgary AB,T6F G9Y"
        lbladress.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lbladress.font = UIFont.systemFont(ofSize: 13)
        lbladress.textAlignment = .left
        self.view.addSubview(lbladress)
       
        let  lblthree :UILabel=UILabel(frame: CGRect(x:78 , y: lbladress.frame.maxY+5, width:30, height:20))
        lblthree.text = "3"
        lblthree.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblthree.font = UIFont.boldSystemFont(ofSize: 10)
        lblthree.textAlignment = .left
        self.view.addSubview(lblthree)
        
        let  lbltow :UILabel=UILabel(frame: CGRect(x:UIScreen.main.bounds.size.width/100*50, y: lbladress.frame.maxY+5, width:30, height:20))
        lbltow.text = "2"
        lbltow.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lbltow.font = UIFont.boldSystemFont(ofSize: 10)
        lbltow.textAlignment = .left
        self.view.addSubview(lbltow)
        
        let  lblcar :UILabel=UILabel(frame: CGRect(x:app.screenWidth-99, y: lbladress.frame.maxY+5, width:60, height:20))
        lblcar.text = "2 Car"
        lblcar.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblcar.font = UIFont.boldSystemFont(ofSize: 10)
        lblcar.textAlignment = .left
        self.view.addSubview(lblcar)
        
        let imgbed = UIImageView(frame: CGRect(x:70, y:lblthree.frame.maxY, width:30, height:20))
        imgbed.image = UIImage(named: "ic_det_bed")
        self.view.addSubview(imgbed)
        
        let imgshower = UIImageView(frame: CGRect(x:UIScreen.main.bounds.size.width/100*48 , y:lbltow.frame.maxY+2, width:25, height:15))
        imgshower.image = UIImage(named: "ic_det_bath")
        self.view.addSubview(imgshower)
        
        let imgcar = UIImageView(frame: CGRect(x:app.screenWidth-98, y:lblcar.frame.maxY, width:20, height:22))
        imgcar.image = UIImage(named: "ic_det_car")
        self.view.addSubview(imgcar)
        
        let middleview = UIView(frame:CGRect(x:0, y: imgcar.frame.maxY, width: app.screenWidth, height:50))
        middleview.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        self.view.addSubview(middleview)
        
        btnmap = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*10, y:5, width:20, height:25))
        btnmap.setImage( UIImage(named: "ic_det_map"), for: UIControlState.normal)
        btnmap.titleLabel?.font=UIFont.systemFont(ofSize: 20)
        middleview.addSubview(btnmap)
        
        btndirection = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*48, y:5, width:25, height:25))
        btndirection.setImage( UIImage(named: "ic_det_direction"), for: UIControlState.normal)
        middleview.addSubview(btndirection)
        
        btnshare = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*81, y:5, width:25, height:25))
        btnshare.setImage( UIImage(named: "ic_det_share"), for: UIControlState.normal)
        btnshare.titleLabel?.font=UIFont.systemFont(ofSize: 20)
        middleview.addSubview(btnshare)
        
        let  lblmap :UILabel=UILabel(frame: CGRect(x:UIScreen.main.bounds.size.width/100*10 , y:btnmap.frame.maxY, width:60, height:10))
        lblmap.text = "Map"
        lblmap.textColor = UIColor.init(red:106/255.0, green: 139/255.0, blue: 160/255.0, alpha: 1.0)
        lblmap.font = UIFont.boldSystemFont(ofSize: 10)
        lblmap.textAlignment = .left
        middleview.addSubview(lblmap)
        
        let  lbldirection :UILabel=UILabel(frame: CGRect(x:UIScreen.main.bounds.size.width/100*38, y: btndirection.frame.maxY, width:100, height:10))
        lbldirection.text = "Directions"
        lbldirection.textColor = UIColor.init(red:106/255.0, green: 139/255.0, blue: 160/255.0, alpha: 1.0)
        lbldirection.font = UIFont.boldSystemFont(ofSize: 10)
        lbldirection.textAlignment = .center
        middleview.addSubview(lbldirection)
        
        let lblshare :UILabel=UILabel(frame: CGRect(x:UIScreen.main.bounds.size.width/100*81, y: btnshare.frame.maxY, width:60, height:10))
        lblshare.text = "Share"
        lblshare.textColor = UIColor.init(red:106/255.0, green: 139/255.0, blue: 160/255.0, alpha: 1.0)
        lblshare.font = UIFont.boldSystemFont(ofSize: 10)
        lblshare.textAlignment = .left
        middleview.addSubview(lblshare)
        
        let lbldes :UILabel=UILabel(frame: CGRect(x: 10, y:middleview.frame.maxY+5, width: app.screenWidth-20, height: 25))
        lbldes.text = "Description:"
        lbldes.textColor = UIColor.black.withAlphaComponent(0.7)
        lbldes.font = UIFont.boldSystemFont(ofSize: 15)
        lbldes.textAlignment = .left
        self.view.addSubview(lbldes)
        
        let lbldes1 :UILabel=UILabel(frame: CGRect(x: 10, y:lbldes.frame.maxY, width: app.screenWidth-20, height: 80))
        lbldes1.text = "Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description"
        lbldes1.textColor = UIColor.black.withAlphaComponent(0.7)
        lbldes1.font = UIFont.systemFont(ofSize: 12)
        lbldes1.textAlignment = .left
        lbldes1.numberOfLines=5
        self.view.addSubview(lbldes1)
        
//        textview = UITextView(frame:CGRect(x: 10, y:middleview.frame.maxY+5, width: app.screenWidth-20, height: 100))
//        textview.textAlignment = .left
//        textview.text = "Description: "
//        textview.delegate = self
//        textview.isEditable = true
//        self.view.addSubview(textview)
        
        loadBottomCollectionview()
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:app.screenHeight/2-80, width: app.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
        
    }
    
    func loadBookviewing()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
       // let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
       // viewbackground.addGestureRecognizer(tapGestureRecognizer)
       // viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)

        popview = UIView(frame: CGRect(x:20, y:80, width:app.screenWidth-40, height:300))
        popview.backgroundColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        popview.layer.cornerRadius = 15.0
        self.view.addSubview(popview)
        
        let lblbooking = UILabel(frame: CGRect(x: 0, y: 0, width:popview.frame.size.width, height: 30))
        lblbooking.text = "Booking Viewing"
        lblbooking.font = UIFont.systemFont(ofSize:18)
        lblbooking.textAlignment = .center
        lblbooking.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        popview.addSubview(lblbooking)
        
        let lbldate = UILabel(frame:CGRect(x: 20, y: lblbooking.frame.maxY+15, width: 45, height: 20))
        lbldate.text = "Day:"
        lbldate.font = UIFont.systemFont(ofSize:15)
        lbldate.textAlignment = .center
        lbldate.textColor=UIColor.black.withAlphaComponent(0.6)
        popview.addSubview(lbldate)
        
        btnmenu = UIButton(frame: CGRect(x:lbldate.frame.maxX+5,y: lblbooking.frame.maxY+10,width:30,height:30))
        btnmenu.setImage(UIImage(named: "ic_calendar_icon"), for: UIControlState.normal)
        btnmenu.addTarget(self, action:#selector(btnmenuclick), for: UIControlEvents.touchUpInside)
        btnmenu.clipsToBounds=true
        popview.addSubview(btnmenu)
        
        let lbltime = UILabel(frame:CGRect(x: 20, y:lbldate.frame.maxY+30, width:120, height: 20))
        lbltime.text = "Showing Time:"
        lbltime.font = UIFont.systemFont(ofSize:15)
        lbltime.textAlignment = .center
        popview.addSubview(lbltime)
        
        let lblform = UILabel(frame:CGRect(x: 50, y:lbltime.frame.maxY+10, width:70, height: 20))
        lblform.text = "From"
        lblform.font = UIFont.systemFont(ofSize:15)
        lblform.textAlignment = .center
        popview.addSubview(lblform)
        
        
        let lblto = UILabel(frame:CGRect(x:popview.frame.size.width-120, y:lbltime.frame.maxY+11, width:40, height: 20))
        lblto.text = "To"
        lblto.font = UIFont.systemFont(ofSize:15)
        lblto.textAlignment = .center
        popview.addSubview(lblto)
        
        
        let Picker  = UIDatePicker(frame: CGRect(x:8,y: lblform.frame.maxY,width:popview.frame.size.width/2-16,height:100))
        Picker.backgroundColor = UIColor.white
        Picker.datePickerMode = UIDatePickerMode.time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        popview.addSubview(Picker)
        
        
        let DatePicker = UIDatePicker(frame:CGRect(x:Picker.frame.maxX+5,y: lblform.frame.maxY,width:popview.frame.size.width/2-10,height:100))
        DatePicker.datePickerMode = UIDatePickerMode.time
        DatePicker.backgroundColor = UIColor.white
        popview.addSubview(DatePicker)
        
        let middleview = UIView(frame: CGRect(x:0, y:popview.frame.size.height-36 ,width:popview.frame.size.width, height:1))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(middleview)
        
        
       let btncancle = UIButton(frame:CGRect(x:5, y:middleview.frame.maxY, width:popview.frame.size.width/2-20, height:30))
        btncancle.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btncancle.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btncancle.addTarget(self, action:#selector(btncancelclick), for: UIControlEvents.touchUpInside)
        btncancle.setTitle("Cancle", for: UIControlState.normal)
        popview.addSubview(btncancle)
        
        
       let btnApply = UIButton(frame:CGRect(x:btncancle.frame.maxX+25 , y:middleview.frame.maxY, width:popview.frame.size.width/2-10, height:30))
        btnApply.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnApply.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btnApply.addTarget(self, action:#selector(btncancelclick), for: UIControlEvents.touchUpInside)
        btnApply.setTitle("Apply", for: UIControlState.normal)
        popview.addSubview(btnApply)
        
        let lineview = UIView(frame: CGRect(x:btncancle.frame.maxX+8, y:popview.frame.size.height-36,width:1, height:36))
        lineview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lineview)

    }
    func loadBottomCollectionview()
    {
        
        viewexpand=UIView(frame: CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if app.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if app.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if app.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-210, width: app.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==1
        {
            let fevriteView = FavriteView(nibName: "FavriteView", bundle: nil)
            navigationController?.pushViewController(fevriteView, animated: true)
        }
        else if indexPath.row==5
        {
            let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
            navigationController?.pushViewController(agentView, animated: true)
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }
        
    }
    
    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }

    
    func btnclick()
    {
        _ = navigationController?.popViewController(animated: true)
       // let agentprofile = agentprofileViewController()
       // self.navigationController?.pushViewController(agentprofile, animated: true)
    }
    
    func btnshowclick(sender:UIButton)
    {
        loadBookviewing()
    }
    
    func btnmenuclick()
    {
        myDatePicker  = UIDatePicker(frame: CGRect(x:120, y:80, width:250, height: 130))
        myDatePicker.datePickerMode = UIDatePickerMode.date
        myDatePicker.backgroundColor = UIColor.white
        myDatePicker.isHidden = false
        myDatePicker.layer.borderColor = UIColor.gray.cgColor
        myDatePicker.layer.borderWidth=1.5
        myDatePicker.layer.cornerRadius = 5.0
       // popview.addSubview(myDatePicker)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneDatePickerPressed))
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
       // popview.addSubview(toolBar)
        
    }
    
    func btncancelclick()
    {
        viewbackground.removeFromSuperview()
        popview.removeFromSuperview()
    }
    
    func doneDatePickerPressed()
    {
        self.view.endEditing(true)
        myDatePicker.removeFromSuperview()
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
