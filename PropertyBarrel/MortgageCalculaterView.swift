//
//  MortgageCalculaterView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/11/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class MortgageCalculaterView: UIViewController,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate {

    var app = UIApplication.shared.delegate as! AppDelegate
    
    var viewback1:UIView!
    var viewexpand:UIView!
    var viewbackground:UIView!
    var viewshare:UIView!
    var viewcal:UIView!
    
    var btnreset     : UIButton!
    var btncalculate : UIButton!
    
    var txtsale  : UITextField!
    var txtdown  : UITextField!
    var txtint   : UITextField!
    var txtamort : UITextField!
    var txtfreq  : UITextField!
    
    var lbl_category_name:UILabel!
    
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selectedlist:Bool!
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]
    var selected:Bool!

    
    @IBInspectable var cornerRadius: CGFloat = 16
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = Int(1.1)
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        if app.selectuser==true
        {
            arr_category_img = ["ic_search","showing1","ic_msg","ic_menu"]
            arr_category_name = ["Property Search","Showing","Messaging","Menu"]
            
            arr_category_img1 = ["ic_search","showing1","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
            arr_category_name1 = ["Property Search","Showing","Messaging","Menu","Mortgage Calculator","Contacts","Map","Suggested Properties","Settings","Share App"]
        }
        selected=false
        loadinitialUI()
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadinitialUI()
    {
        let lblcal = UILabel(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: 50))
        lblcal.text = "Mortgage Calculator"
        lblcal.font = UIFont.boldSystemFont(ofSize: 17)
        lblcal.textAlignment = .center
        lblcal.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(lblcal)
        
//        let backview = UIView(frame: CGRect(x:20, y:lblcal.frame.maxY,width:app.screenWidth-40, height:app.screenHeight/2+65))
//        backview.layer.cornerRadius = 15.0
//        backview.layer.shadowColor = UIColor.white.cgColor
//        backview.backgroundColor = UIColor.init(red:255/255.0, green: 255/255.0, blue:255/255.0, alpha: 1.0)
//        self.view.addSubview(backview)
        
        viewback1=UIView(frame: CGRect(x: 21, y:lblcal.frame.maxY+5, width: app.screenWidth-42, height:355))
        viewback1.backgroundColor=UIColor.white
        viewback1.layer.cornerRadius=15
        //  viewback1.tag=indexPath.row
        self.view.addSubview(viewback1)
        
        viewback1.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: viewback1.bounds, cornerRadius: cornerRadius)
        viewback1.layer.masksToBounds = false
        viewback1.layer.shadowColor = shadowColor?.cgColor
        viewback1.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewback1.layer.shadowOpacity = shadowOpacity
        viewback1.layer.shadowPath = shadowPath.cgPath

        let topview = UIView(frame: CGRect(x:20, y:lblcal.frame.maxY, width:app.screenWidth-40, height:361))
        topview.backgroundColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        topview.layer.cornerRadius = 15.0
       // topview.layer.shadowColor = UIColor.black.cgColor
       // topview.layer.shadowOpacity = 0.4
       // topview.layer.shadowRadius = 3
        self.view.addSubview(topview)
        
        let lblsale = UILabel(frame: CGRect(x:5 , y: 5, width:100, height:25))
        lblsale.text = "Sale Price:"
        lblsale.font = UIFont.boldSystemFont(ofSize: 15)
        lblsale.textAlignment = .center
        lblsale.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        topview.addSubview(lblsale)
        
        btnreset = UIButton(frame:CGRect(x:topview.frame.size.width-50, y:5, width:40, height:25))
        btnreset.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnreset.titleLabel?.font  = UIFont.boldSystemFont(ofSize: 13)
        btnreset.setTitle("Reset", for: UIControlState.normal)
        topview.addSubview(btnreset)
        
        txtsale = UITextField(frame: CGRect(x:10,y:lblsale.frame.maxY,width:topview.frame.size.width-20,height:30))
        txtsale.backgroundColor = UIColor.white
        txtsale.layer.cornerRadius = 5.0
        txtsale.layer.borderWidth=1.5
        txtsale.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtsale.borderStyle = UITextBorderStyle.roundedRect
        txtsale.keyboardType = .decimalPad
        txtsale.delegate=self
        topview.addSubview(txtsale)
        
        let lbldown = UILabel(frame: CGRect(x:10, y: txtsale.frame.maxY, width:120, height: 30))
        lbldown.text = "Down Payment:"
        lbldown.font = UIFont.boldSystemFont(ofSize: 15)
        lbldown.textAlignment = .center
        lbldown.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        topview.addSubview(lbldown)
        
        txtdown = UITextField(frame: CGRect(x:10,y:lbldown.frame.maxY,width:topview.frame.size.width-20,height:30))
        txtdown.layer.cornerRadius = 5.0
        txtdown.layer.borderWidth=1.5
        txtdown.borderStyle = UITextBorderStyle.roundedRect
        txtdown.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtdown.delegate=self
        txtdown.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtdown.keyboardType = .decimalPad
        topview.addSubview(txtdown)
        
        
        let lblinterest = UILabel(frame: CGRect(x: 10, y:txtdown.frame.maxY+5, width:100, height: 30))
        lblinterest.text = "Interest Rate:"
        lblinterest.font = UIFont.boldSystemFont(ofSize:15)
        lblinterest.textAlignment = .center
        lblinterest.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        topview.addSubview(lblinterest)
        
        txtint = UITextField(frame: CGRect(x:10,y: lblinterest.frame.maxY,width:topview.frame.size.width-20,height:30))
        txtint.backgroundColor = UIColor.white
        txtint.layer.borderWidth=1.5
        txtint.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtint.borderStyle = UITextBorderStyle.roundedRect
        txtint.layer.cornerRadius = 5.0
        txtint.delegate=self
        txtint.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtint.keyboardType = .decimalPad
        topview.addSubview(txtint)
        
        let lblamort = UILabel(frame: CGRect(x:10, y:txtint.frame.maxY, width:100, height: 30))
        lblamort.text = "Amortization:"
        lblamort.font = UIFont.boldSystemFont(ofSize: 15)
        lblamort.textAlignment = .center
        lblamort.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        topview.addSubview(lblamort)
        
        txtamort = UITextField(frame: CGRect(x:10,y:lblamort.frame.maxY,width:topview.frame.size.width-20,height:30))
        txtamort.delegate=self
        txtamort.layer.borderWidth=1.5
        txtamort.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtamort.borderStyle = UITextBorderStyle.roundedRect
        txtamort.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtamort.layer.cornerRadius = 5.0
        txtamort.keyboardType = .decimalPad
        topview.addSubview(txtamort)
        
        let lblfreq = UILabel(frame: CGRect(x: 10, y: txtamort.frame.maxY, width:90, height: 30))
        lblfreq.text = "Frequency:"
        lblfreq.font = UIFont.boldSystemFont(ofSize: 15)
        lblfreq.textAlignment = .center
        lblfreq.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        topview.addSubview(lblfreq)
        
        txtfreq = UITextField(frame: CGRect(x:10,y:lblfreq.frame.maxY,width:topview.frame.size.width-20,height:30))
        txtfreq.backgroundColor = UIColor.white
        txtfreq.layer.cornerRadius = 5.0
        txtfreq.layer.borderWidth=1.5
        txtfreq.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtfreq.borderStyle = UITextBorderStyle.roundedRect
        txtfreq.delegate=self
        txtfreq.keyboardType = .decimalPad
        topview.addSubview(txtfreq)
        
        let middleview = UIView(frame: CGRect(x:0, y: txtfreq.frame.maxY+20,width:topview.frame.size.width, height:1))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        topview.addSubview(middleview)
        
        btncalculate = UIButton(frame:CGRect(x:0, y:middleview.frame.maxY+5, width:topview.frame.size.width, height:25))
        btncalculate.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btncalculate.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btncalculate.setTitle("Calculate", for: UIControlState.normal)
        btncalculate.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(btncalculate)
        
        // datepicker toolbar setup
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donephonePressed))
        
        // if you remove the space element, the "done" button will be left aligned
        // you can add more items if you want
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        txtsale.inputAccessoryView = toolBar
        txtdown.inputAccessoryView = toolBar
        txtint.inputAccessoryView = toolBar
        txtamort.inputAccessoryView = toolBar
        txtfreq.inputAccessoryView = toolBar

        loadBottomCollectionview()
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:app.screenHeight/2-80, width: app.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
        
    }
    
    func loadPopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewbackground)
        
        viewcal=UIView(frame: CGRect(x: 15, y:app.screenHeight/2-175, width: app.screenWidth-30, height: 310))
        viewcal.backgroundColor=UIColor.white
        viewcal.layer.cornerRadius=14
        self.view.addSubview(viewcal)
        
        let lblmonpay = UILabel(frame: CGRect(x:15 , y: 15, width:viewcal.frame.size.width-30, height:25))
        lblmonpay.text = "Monthly Payment"
        lblmonpay.font = UIFont.boldSystemFont(ofSize: 15)
        lblmonpay.textAlignment = .left
        lblmonpay.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewcal.addSubview(lblmonpay)
        
        let lblmonprice = UILabel(frame: CGRect(x:15 , y: lblmonpay.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lblmonprice.text = "$1,054.01"
        lblmonprice.font = UIFont.boldSystemFont(ofSize: 20)
        lblmonprice.textAlignment = .left
        lblmonprice.textColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        viewcal.addSubview(lblmonprice)
        
        let lbltotpay = UILabel(frame: CGRect(x:15 , y: lblmonprice.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lbltotpay.text = "Total Payments"
        lbltotpay.font = UIFont.boldSystemFont(ofSize: 15)
        lbltotpay.textAlignment = .left
        lbltotpay.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewcal.addSubview(lbltotpay)
        
        let lbltotprice = UILabel(frame: CGRect(x:15 , y: lbltotpay.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lbltotprice.text = "$379,443.01"
        lbltotprice.font = UIFont.boldSystemFont(ofSize: 20)
        lbltotprice.textAlignment = .left
        lbltotprice.textColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        viewcal.addSubview(lbltotprice)
        
        let lbltotint = UILabel(frame: CGRect(x:15 , y: lbltotprice.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lbltotint.text = "Total Interest Paid"
        lbltotint.font = UIFont.boldSystemFont(ofSize: 15)
        lbltotint.textAlignment = .left
        lbltotint.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewcal.addSubview(lbltotint)
        
        let lbltotpaid = UILabel(frame: CGRect(x:15 , y: lbltotint.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lbltotpaid.text = "$129,443.01"
        lbltotpaid.font = UIFont.boldSystemFont(ofSize: 20)
        lbltotpaid.textAlignment = .left
        lbltotpaid.textColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        viewcal.addSubview(lbltotpaid)
        
        let lblpayoff = UILabel(frame: CGRect(x:15 , y: lbltotpaid.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lblpayoff.text = "Payoff Date"
        lblpayoff.font = UIFont.boldSystemFont(ofSize: 15)
        lblpayoff.textAlignment = .left
        lblpayoff.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewcal.addSubview(lblpayoff)
        
        let lblpayof = UILabel(frame: CGRect(x:15 , y: lblpayoff.frame.maxY+5, width:viewcal.frame.size.width-30, height:25))
        lblpayof.text = "March 2048"
        lblpayof.font = UIFont.boldSystemFont(ofSize: 20)
        lblpayof.textAlignment = .left
        lblpayof.textColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        viewcal.addSubview(lblpayof)


        let middleview = UIView(frame: CGRect(x:0, y: lblpayof.frame.maxY+15,width:viewcal.frame.size.width, height:1))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewcal.addSubview(middleview)

      let  btnclose = UIButton(frame:CGRect(x:0, y:middleview.frame.maxY+10, width:viewcal.frame.size.width, height:25))
        btnclose.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnclose.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btnclose.setTitle("Close", for: UIControlState.normal)
        btnclose.addTarget(self, action:#selector(btnclick1), for: UIControlEvents.touchUpInside)
        viewcal.addSubview(btnclose)

    }

    
    func loadBottomCollectionview()
    {
        
        viewexpand=UIView(frame: CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if app.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if app.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if app.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-210, width: app.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==1
        {
            if app.selectuser==true
            {
                let showingView = ShowingView(nibName: "ShowingView", bundle: nil)
                navigationController?.pushViewController(showingView, animated: true)
            }
            else
            {
                let fevriteView = FavriteView(nibName: "FavriteView", bundle: nil)
                navigationController?.pushViewController(fevriteView, animated: true)
            }
        }
        else if indexPath.row==5
        {
            if app.selectuser==true
            {
                let contactView = ContactsView(nibName: "ContactsView", bundle: nil)
                navigationController?.pushViewController(contactView, animated: true)
            }
            else
            {
                let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
                navigationController?.pushViewController(agentView, animated: true)
            }
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==7
        {
            let suggestedView = SuggestedProperties(nibName: "SuggestedProperties", bundle: nil)
            navigationController?.pushViewController(suggestedView, animated: true)
        }
        else if indexPath.row==8
        {
            let settingView = SettingsView(nibName: "SettingsView", bundle: nil)
            navigationController?.pushViewController(settingView, animated: true)
        }
        
    }

    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }

    func btnclick(sender:UIButton)
    {
        loadPopupUI()
    }
    
    func btnclick1(sender:UIButton)
    {
        viewbackground.removeFromSuperview()
        viewcal.removeFromSuperview()
    }
    
    func donephonePressed()
    {
        self.view.endEditing(true)
    }
    //UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
