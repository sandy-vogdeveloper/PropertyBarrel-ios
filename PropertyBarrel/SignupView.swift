//
//  SignupView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/11/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class SignupView: UIViewController,UITextFieldDelegate {
    var app = UIApplication.shared.delegate as! AppDelegate
    var btnlogin:UIButton!
    var btnfb : UIButton!
    var btnforgot : UIButton!
    var btnsign : UIButton!
    var btnagent : UIButton!
    var btnhome : UIButton!
    
    var txtusername: UITextField!
    var txtpass:UITextField!
    var txtcpass:UITextField!
    var imgback : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self
            .keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        loadinitialUI()
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadinitialUI()
    {
        let imgview = UIImageView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:app.screenHeight))
        imgview.image = UIImage(named: "im_bg")
        self.view.addSubview(imgview)
        
        let imgcircle = UIImageView(frame: CGRect(x:25,y:10,width:app.screenWidth-50,height:app.screenWidth-50))
        imgcircle.image = UIImage(named: "logo1")
        imgcircle.clipsToBounds=true
        imgview.addSubview(imgcircle)
        
        btnlogin = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*25, y:imgcircle.frame.maxY+5, width:app.screenWidth/2, height:35))
        btnlogin.setTitleColor(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0), for: UIControlState.normal)
        btnlogin.setTitle("Sign Up", for: UIControlState.normal)
        btnlogin.titleLabel?.font=UIFont.boldSystemFont(ofSize: 22)
        btnlogin.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btnlogin)
        
        let topview = UIView(frame: CGRect(x: 15,y:btnlogin.frame.maxY+5, width: app.screenWidth-30, height:205))
        topview.backgroundColor = UIColor.init(red: 204/255.0, green:204/255.0, blue: 204/255.0, alpha: 1.0)
        //topview.layer.shadowRadius = 5.0
        //topview.layer.shadowOpacity = 0.4
        topview.layer.cornerRadius = 15.0
        self.view.addSubview(topview)
        
        txtusername = UITextField(frame: CGRect(x:10,y:10,width:topview.frame.size.width-20,height:40))
        txtusername.attributedPlaceholder = NSAttributedString(string: "  Enter Username ",attributes: [NSForegroundColorAttributeName: UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)])
        txtusername.font = UIFont.systemFont(ofSize: 20)
        txtusername.keyboardType = .emailAddress
        txtusername.backgroundColor = UIColor.white
        txtusername.layer.cornerRadius = 5.0
        txtusername.delegate=self
        txtusername.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtusername.addTarget(self, action:#selector(dismissKeyboard), for:UIControlEvents.editingDidEndOnExit)
        topview.addSubview(txtusername)
        
        
        txtpass = UITextField(frame: CGRect(x:10,y:txtusername.frame.maxY+10,width:topview.frame.size.width-20,height:40))
        txtpass.attributedPlaceholder = NSAttributedString(string: "  Enter Password ",attributes: [NSForegroundColorAttributeName: UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)])
        txtpass.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtpass.backgroundColor = UIColor.white
        txtpass.isSecureTextEntry=true
        txtpass.layer.cornerRadius = 5.0
        txtpass.delegate=self
        
        txtpass.addTarget(self, action:#selector(dismissKeyboard), for:UIControlEvents.editingDidEndOnExit)
        topview.addSubview(txtpass)
        
        txtcpass = UITextField(frame: CGRect(x:10,y:txtpass.frame.maxY+10,width:topview.frame.size.width-20,height:40))
        txtcpass.attributedPlaceholder = NSAttributedString(string: "  Confirm Password ",attributes: [NSForegroundColorAttributeName: UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)])
        txtcpass.layer.cornerRadius = 5.0
        txtcpass.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtcpass.backgroundColor = UIColor.white
        txtcpass.delegate=self
        txtcpass.addTarget(self, action:#selector(dismissKeyboard), for:UIControlEvents.editingDidEndOnExit)
        txtcpass.isSecureTextEntry=true
        topview.addSubview(txtcpass)
        
        let lblagent = UILabel(frame: CGRect(x:25, y:  txtcpass.frame.maxY+15, width:70, height: 30))
        lblagent.text = "Agent"
        lblagent.font=UIFont.systemFont(ofSize:18)
        lblagent.textAlignment = .center
        lblagent.sizeToFit()
        lblagent.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        topview.addSubview(lblagent)
        
        btnagent = UIButton(frame:CGRect(x:lblagent.frame.maxX+7, y: txtcpass.frame.maxY+15, width:25, height: 25))
        btnagent.setTitleColor(UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0), for: UIControlState.normal)
        btnagent.setImage (UIImage(named: "circle-outline"), for: UIControlState.normal)
        btnagent.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(btnagent)
        
        let lblhome = UILabel(frame: CGRect(x:topview.frame.size.width-155, y:  txtcpass.frame.maxY+11, width:100, height: 30))
        lblhome.text = "Home Buyer"
        lblhome.font=UIFont.systemFont(ofSize:18)
        lblhome.textAlignment = .center
        lblhome.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        topview.addSubview(lblhome)
        
        btnhome = UIButton(frame:CGRect(x:lblhome.frame.maxX+5, y: txtcpass.frame.maxY+15, width:25, height: 25))
        btnhome.setTitleColor(UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0), for: UIControlState.normal)
        btnhome.setImage(UIImage(named: "circle-outline"), for: UIControlState.normal)
        btnhome.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(btnhome)
        
        btnsign = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*25, y:topview.frame.maxY+5, width:app.screenWidth/2, height:30))
        btnsign.setTitleColor(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0), for: UIControlState.normal)
        btnsign.setTitle(" Login ", for: UIControlState.normal)
        btnsign.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnsign.titleLabel?.font=UIFont.boldSystemFont(ofSize:17)
        self.view.addSubview(btnsign)
        
    }
    
    func dismissKeyboard()
    {
        
    }
    
    func btnclick()
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    //Keyboard Show & Hide methods
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
