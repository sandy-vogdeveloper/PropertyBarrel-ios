//
//  SettingsView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/10/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class SettingsView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    var app = UIApplication.shared.delegate as! AppDelegate
    var viewexpand:UIView!
    var viewback1:UIView!
    var viewbackground:UIView!
    var viewshare:UIView!
    var btnlogout:UIButton!
    var lbl_category_name:UILabel!
    
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selectedlist:Bool!
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]
    var selected:Bool!

    @IBInspectable var cornerRadius: CGFloat = 16
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = Int(1.1)
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        
        if app.selectuser==true
        {
            arr_category_img = ["ic_search","showing1","ic_msg","ic_menu"]
            arr_category_name = ["Property Search","Showing","Messaging","Menu"]
            
            arr_category_img1 = ["ic_search","showing1","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
            arr_category_name1 = ["Property Search","Showing","Messaging","Menu","Mortgage Calculator","Contacts","Map","Suggested Properties","Settings","Share App"]
        }
        selected=false
        loadinitialUI()
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    func loadinitialUI()
    {
        let lblsetting = UILabel(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: 40))
        lblsetting.text = "Settings"
        lblsetting.font = UIFont.systemFont(ofSize: 20)
        lblsetting.textAlignment = .center
        lblsetting.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        self.view.addSubview(lblsetting)
        
        viewback1=UIView(frame: CGRect(x: 21, y:60, width: app.screenWidth-42, height:app.screenHeight/2-45))
        viewback1.backgroundColor=UIColor.white
        viewback1.layer.cornerRadius=15
        //  viewback1.tag=indexPath.row
        self.view.addSubview(viewback1)
        
        viewback1.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: viewback1.bounds, cornerRadius: cornerRadius)
        viewback1.layer.masksToBounds = false
        viewback1.layer.shadowColor = shadowColor?.cgColor
        viewback1.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewback1.layer.shadowOpacity = shadowOpacity
        viewback1.layer.shadowPath = shadowPath.cgPath

        let topview = UIView(frame: CGRect(x: 20, y:55, width: app.screenWidth-40, height:app.screenHeight/2-40))
        topview.backgroundColor = UIColor.white
        topview.layer.cornerRadius = 15.0
        self.view.addSubview(topview)
        
        let lblallow = UILabel(frame: CGRect(x:0, y: 5, width: topview.frame.size.width, height: 50))
        lblallow.textColor = UIColor.init(red:134/255.0, green: 134/255.0, blue:
            134/255.0, alpha: 1.0)
        lblallow.text  = "Allow Push Notifications"
        lblallow.font = UIFont.systemFont(ofSize: 18)
        lblallow.textAlignment = .center
        topview.addSubview(lblallow)
        
        let lblyes = UILabel(frame: CGRect(x:topview.frame.size.width/2-75, y:lblallow.frame.maxY+29, width:60, height: 20))
        lblyes.text  = "Yes"
        lblyes.textColor = UIColor.init(red:134/255.0, green: 134/255.0, blue:134/255.0, alpha: 1.0)
        lblyes.textAlignment = .left
        topview.addSubview(lblyes)
        
        let myswitch =  UISwitch(frame: CGRect(x: topview.frame.size.width/2-25, y: lblallow.frame.maxY+25, width:50, height: 10))
        myswitch.onTintColor = UIColor.init(red:42/255.0, green: 80/255.0, blue: 114/255.0, alpha: 1.0)
        topview.addSubview(myswitch)
        
        let lblno = UILabel(frame: CGRect(x:topview.frame.size.width/2+50, y:lblallow.frame.maxY+30, width:40, height: 20))
        lblno.text  = "No"
        lblno.textColor = UIColor.init(red:134/255.0, green: 134/255.0, blue:134/255.0, alpha: 1.0)
        lblno.textAlignment = .left
        topview.addSubview(lblno)
        
        btnlogout = UIButton(frame:CGRect(x:5, y:app.screenHeight/2-100, width: app.screenWidth-53, height:34))
        btnlogout.setTitle("Logout", for: UIControlState.normal)
        btnlogout.titleLabel?.font=UIFont.systemFont(ofSize: 18)
        btnlogout.layer.cornerRadius = 7.0
        btnlogout.backgroundColor = UIColor.init(red: 42/255.0, green: 80/255.0, blue: 114/255.0, alpha: 1.0)
        btnlogout.setTitleColor(UIColor.init(red: 253/255.0, green: 253/255.0, blue:253/255.0, alpha: 1.0), for: UIControlState.normal)
        btnlogout.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnlogout.titleLabel?.textAlignment = .center
        topview.addSubview(btnlogout)
    
        loadBottomCollectionview()
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:app.screenHeight/2-80, width: app.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
        
    }

    
    func loadBottomCollectionview()
    {
        
        viewexpand=UIView(frame: CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if app.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if app.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if app.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-210, width: app.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==1
        {
            if app.selectuser==true
            {
                let showingView = ShowingView(nibName: "ShowingView", bundle: nil)
                navigationController?.pushViewController(showingView, animated: true)
            }
            else
            {
                let fevriteView = FavriteView(nibName: "FavriteView", bundle: nil)
                navigationController?.pushViewController(fevriteView, animated: true)
            }
        }
        else if indexPath.row==5
        {
            if app.selectuser==true
            {
                let contactView = ContactsView(nibName: "ContactsView", bundle: nil)
                navigationController?.pushViewController(contactView, animated: true)
            }
            else
            {
                let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
                navigationController?.pushViewController(agentView, animated: true)
            }
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }
        else if indexPath.row==7
        {
            let suggestedView = SuggestedProperties(nibName: "SuggestedProperties", bundle: nil)
            navigationController?.pushViewController(suggestedView, animated: true)
        }
        
    }

    func btnclick()
    {
        let loginView = LoginView(nibName: "LoginView", bundle: nil)
        navigationController?.pushViewController(loginView, animated: true)
    }

    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
