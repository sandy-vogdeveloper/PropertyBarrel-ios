//
//  AgentProfileView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/13/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class AgentProfileView: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource {
    var app = UIApplication.shared.delegate as! AppDelegate
    
    var viewexpand:UIView!
    var viewshare:UIView!
    var viewbackground:UIView!
    
    var btnback: UIButton!
    var btncall: UIButton!
    var btnsms:  UIButton!
    var tableview : UITableView!
    var arrname : NSMutableArray!
    var arremail :NSMutableArray!
    var imgprofile:UIImageView!
    var imgback : UIImageView!
    
    var lbl_category_name:UILabel!
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selectedlist:Bool!
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]
    var selected:Bool!

    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        selected=false
        loadinitialUI()
        loadarray()
        
    }
    func loadarray()
    {
        arrname = [ "Name:","Email:","Brokerage:","Phone:","Location:"]
        arremail = ["Ashley Jenkins","ashleyj@greathouse.com","Great House Realty","1-403-555-4444","Calgary, AB"]
    }
    func loadinitialUI()
    {
        let topview = UIView(frame:CGRect(x: 0, y: 0, width: app.screenWidth, height: 30))
        topview.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        self.view.addSubview(topview)
        
       let imgback1 = UIButton(frame: CGRect(x:5, y: 5, width:20, height:20))
        imgback1.setBackgroundImage(UIImage(named:"ic_det_back"), for: .normal)
        imgback1.alpha=0.6
         imgback1.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(imgback1)
        
        btnback = UIButton(frame:CGRect(x:imgback1.frame.maxX, y:5, width:50, height:20))
        btnback.setTitleColor(UIColor.init(red: 202/255.0, green: 202/255.0, blue: 202/255.0, alpha: 1.0), for: UIControlState.normal)
        btnback.setTitle("Back", for: UIControlState.normal)
        btnback.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnback.titleLabel?.font=UIFont.systemFont(ofSize: 17)
        topview.addSubview(btnback)
        
        
        let lblprofile = UILabel(frame: CGRect(x:btnback.frame.maxX+5, y: 5 , width: app.screenWidth-190, height: 20))
        lblprofile.text = "Profile"
        lblprofile.font = UIFont.systemFont(ofSize:17)
        lblprofile.textColor = UIColor.init(red: 202/255.0, green: 202/255.0, blue: 202/255.0, alpha: 1.0)
        lblprofile.textAlignment = .center
        topview.addSubview(lblprofile)
        
        
        btnsms = UIButton(frame:CGRect(x:app.screenWidth-60, y:5, width:20, height:20))
        btnsms.setImage( UIImage(named: "ic_dettop_msg1"), for: UIControlState.normal)
        btnsms.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnsms.alpha=0.6
        topview.addSubview(btnsms)
        
        
        btncall = UIButton(frame:CGRect(x:app.screenWidth-30, y:5, width:20, height:20))
        btncall.setImage( UIImage(named: "ic_dettop_phone"), for: UIControlState.normal)
        btncall.alpha=0.6
        topview.addSubview(btncall)
        
        imgprofile = UIImageView(frame: CGRect(x: 0, y:topview.frame.maxY, width: app.screenWidth, height:app.screenHeight/2-120))
        imgprofile.image = UIImage(named: "agent_pic")
        self.view.addSubview(imgprofile)
        
        
        tableview = UITableView(frame: CGRect(x: 0, y:imgprofile.frame.maxY, width: app.screenWidth, height: app.screenHeight/2))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.estimatedRowHeight = 50.0
        tableview.separatorStyle = UITableViewCellSeparatorStyle.none
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableview)
        
        loadBottomCollectionview()
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:app.screenHeight/2-80, width: app.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
        
    }

    
    func loadBottomCollectionview()
    {
        
        viewexpand=UIView(frame: CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if app.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if app.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if app.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-80, width: app.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: app.screenHeight-210, width: app.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:app.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==1
        {
            let fevriteView = FavriteView(nibName: "FavriteView", bundle: nil)
            navigationController?.pushViewController(fevriteView, animated: true)
        }
        else if indexPath.row==5
        {
            let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
            navigationController?.pushViewController(agentView, animated: true)
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }
        
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrname.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let lblname:UILabel = UILabel()
        lblname.frame = CGRect (x: 10, y:3, width: app.screenWidth, height: 20)
        lblname.text = arrname [indexPath.row] as? String
        lblname.textColor = UIColor.init(red: 131/255.0, green: 131/255.0, blue:131/255.0, alpha: 1.0)
        lblname.font = UIFont.boldSystemFont(ofSize: 12)
        cell.addSubview(lblname)
        
        let lblemail:UILabel = UILabel()
        lblemail.frame = CGRect (x: 10, y:lblname.frame.maxY, width: app.screenWidth, height: 20)
        lblemail.text = arremail[indexPath.row] as? String
        lblemail.textColor = UIColor.init(red: 55/255.0, green: 104/255.0, blue:133/255.0, alpha: 1.0)
        lblemail.font = UIFont.boldSystemFont(ofSize:15)
        cell.addSubview(lblemail)
        
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = UIColor.white
        }
            
        else{
            cell.backgroundColor = UIColor.init(red: 242/255.0, green:242/255.0, blue: 242/255.0, alpha: 1.0)
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return   50
    }
    
    func btnclick()
    {
       _ = navigationController?.popViewController(animated: true)
    }
    
    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
