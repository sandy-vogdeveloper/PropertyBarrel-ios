//
//  LoginView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/10/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class LoginView: UIViewController,UITextFieldDelegate {

    var app = UIApplication.shared.delegate as! AppDelegate
    var btnlogin  : UIButton!
    var btnfb     : UIButton!
    var btnforgot : UIButton!
    var btnsign   : UIButton!
    
    var txtusername:     UITextField!
    var txtpass    :     UITextField!
    
    var imgback    :     UIImageView!
    var img_Backgrund:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self
            .keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        self.view.backgroundColor = UIColor.white
        loadinitialUI()
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    
    func loadinitialUI()
    {
        
        img_Backgrund=UIImageView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        img_Backgrund.image=UIImage(named: "im_bg")
        img_Backgrund.clipsToBounds=true
       // img_Backgrund.alpha=0.5
        self.view.addSubview(img_Backgrund)
        
        let imgcircle = UIImageView(frame: CGRect(x:25,y:10,width:app.screenWidth-50,height:app.screenWidth-50))
        imgcircle.image = UIImage(named: "logo1")
        imgcircle.clipsToBounds=true
        self.view.addSubview(imgcircle)
        
        btnlogin = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*25, y:imgcircle.frame.maxY+5, width:app.screenWidth/2, height:35))
        btnlogin.setTitleColor(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0), for: UIControlState.normal)
        btnlogin.setTitle("Login", for: UIControlState.normal)
        btnlogin.addTarget(self, action:#selector(btnclick1), for: UIControlEvents.touchUpInside)
        btnlogin.titleLabel?.font=UIFont.systemFont(ofSize:25)
        self.view.addSubview(btnlogin)
        
        let topview = UIView(frame: CGRect(x: 15, y:btnlogin.frame.maxY+5, width: app.screenWidth-30, height:190))
        topview.backgroundColor = UIColor.init(red: 204/255.0, green:204/255.0, blue: 204/255.0, alpha: 1.0)
        topview.layer.cornerRadius = 15.0
        //topview.layer.shadowRadius = 3
       // topview.layer.shadowOpacity = 0.4
        self.view.addSubview(topview)
        
        txtusername = UITextField(frame: CGRect(x:10,y:10,width:topview.frame.size.width-20,height:40))
        txtusername.attributedPlaceholder = NSAttributedString(string: "  Username ",attributes: [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)])
        txtusername.backgroundColor = UIColor.white
        txtusername.keyboardType = .emailAddress
        txtusername.layer.cornerRadius = 5.0
        txtusername.delegate=self
        txtusername.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtusername.addTarget(self, action:#selector(dismissKeyboard), for:UIControlEvents.editingDidEndOnExit)
        topview.addSubview(txtusername)
        
        txtpass = UITextField(frame: CGRect(x:10,y:txtusername.frame.maxY+10,width:topview.frame.size.width-20,height:40))
        txtpass.attributedPlaceholder = NSAttributedString(string: "  Password ",attributes: [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)])
        txtpass.layer.cornerRadius = 5.0
        txtpass.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        txtpass.backgroundColor = UIColor.white
        txtpass.delegate=self
        txtpass.addTarget(self, action:#selector(dismissKeyboard), for:UIControlEvents.editingDidEndOnExit)
        txtpass.isSecureTextEntry=true
        topview.addSubview(txtpass)
        
        btnfb = UIButton(frame:CGRect(x:10, y:txtpass.frame.maxY+10, width:topview.frame.size.width-20, height:40))
        btnfb.setBackgroundImage(UIImage(named:"btnfb"), for: .normal)
        btnfb.addTarget(self, action:#selector(btnclick3), for: UIControlEvents.touchUpInside)
        //btnfb.titleLabel?.font=UIFont.systemFont(ofSize:20)
        topview.addSubview(btnfb)
        
        btnforgot = UIButton(frame:CGRect(x:10, y:btnfb.frame.maxY+5, width:topview.frame.size.width-20, height:25))
        btnforgot.setTitleColor(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0), for: UIControlState.normal)
        btnforgot.setTitle("Forgot Password ", for: UIControlState.normal)
        btnforgot.addTarget(self, action:#selector(btnclick1), for: UIControlEvents.touchUpInside)
        btnforgot.titleLabel?.font=UIFont.systemFont(ofSize:15)
        topview.addSubview(btnforgot)
        
        btnsign = UIButton(frame:CGRect(x:UIScreen.main.bounds.size.width/100*25, y:topview.frame.maxY+10, width:app.screenWidth/2, height:40))
        btnsign.setTitleColor(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0), for: UIControlState.normal)
        btnsign.setTitle("Sign Up ", for: UIControlState.normal)
        btnsign.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnsign.titleLabel?.font=UIFont.systemFont(ofSize:15)
        self.view.addSubview(btnsign)
        
    }
    
    func dismissKeyboard()
    {
        
    }
    
    func btnclick1()
    {
       _ = navigationController?.popViewController(animated: true)
    }
    
    func btnclick()
    {
        let signup = SignupView(nibName: "SignupView", bundle: nil)
        self.navigationController?.pushViewController(signup, animated: true)
    }
    
    func btnclick3()
    {
        app.selectuser=true
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login") as UIViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)

       // _ = navigationController?.popToRootViewController(animated: true)
    }

    //UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    //Keyboard Show & Hide methods
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -110
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
