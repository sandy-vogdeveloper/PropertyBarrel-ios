//
//  FavriteView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/5/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit
@IBDesignable
class FavriteView: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate {
    var appDelegate:AppDelegate!
    var viewtop:UIView!
    var viewexpand:UIView!
    var viewbackground:UIView!
    var viewshare:UIView!
    
    var img_Backgrund:UIImageView!
    var lbl_category_name:UILabel!
    var lbl_category_name1:UILabel!
    var lbl_category:UILabel!
    var lbltitle1:UILabel!
    
    var tbl_listview1=UITableView()
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selectedlist:Bool!
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]
    var selected:Bool!
    
    @IBInspectable var cornerRadius: CGFloat = 16
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = Int(1.1)
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    var cellleft:Bool!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        selected=false
        selectedlist=false
        cellleft=false
        loadInitialUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
    }
    
    func loadInitialUI()
    {
//        img_Backgrund=UIImageView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
//        img_Backgrund.image=UIImage(named: "im_bg")
//        img_Backgrund.clipsToBounds=true
//        img_Backgrund.alpha=0.2
//        self.view.addSubview(img_Backgrund)
        self.view.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        viewtop=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: 45))
        viewtop.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.view.addSubview(viewtop)
        
        lbl_category_name1=UILabel(frame: CGRect(x: 0, y:20, width: appDelegate.screenWidth, height: 25))
        lbl_category_name1.text="Favorites"
        lbl_category_name1.textColor=UIColor.gray
        lbl_category_name1.font=UIFont.boldSystemFont(ofSize: 16)
        lbl_category_name1.textAlignment = .center
        viewtop.addSubview(lbl_category_name1)
        
        let imgremove=UIImageView(frame: CGRect(x: 15, y: viewtop.frame.maxY+5, width: 20, height: 20))
        imgremove.image=UIImage(named: "deletedark")
        self.view.addSubview(imgremove)
        
        let imgshowing=UIImageView(frame: CGRect(x: appDelegate.screenWidth-35, y: viewtop.frame.maxY+5, width: 20, height: 20))
        imgshowing.image=UIImage(named: "ic_dettop_showing")
        self.view.addSubview(imgshowing)

        let imgarrow=UIImageView(frame: CGRect(x: imgremove.frame.maxX+5, y: viewtop.frame.maxY+10, width: 50, height: 10))
        imgarrow.image=UIImage(named: "leftarrow")
        self.view.addSubview(imgarrow)
        
        lbl_category=UILabel(frame: CGRect(x: appDelegate.screenWidth/2-100, y:viewtop.frame.maxY+2, width: 200, height: 25))
        lbl_category.text="Request Showing or Remove"
        lbl_category.textColor=UIColor.gray
        lbl_category.font=UIFont.systemFont(ofSize: 11)
        lbl_category.textAlignment = .center
        self.view.addSubview(lbl_category)
        
        let imgarrow1=UIImageView(frame: CGRect(x: appDelegate.screenWidth-90, y: viewtop.frame.maxY+10, width: 50, height: 10))
        imgarrow1.image=UIImage(named: "rightarrow")
        self.view.addSubview(imgarrow1)

        if appDelegate.screenWidth==320
        {
            imgarrow.frame=CGRect(x: imgremove.frame.maxX+5, y: viewtop.frame.maxY+10, width: 45, height: 10)
            imgarrow1.frame=CGRect(x: appDelegate.screenWidth-85, y: viewtop.frame.maxY+10, width: 45, height: 10)
        }
        loadTableviewlist()
        loadBottomCollectionview()
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-80, width: appDelegate.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
    }
    
    func loadRemovePopup()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewbackground)

        
        viewshare=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-70, width: appDelegate.screenWidth-30, height: 140))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)

        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 25))
        lbltitle.text="Remove Listing?"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        if appDelegate.screenWidth==320
        {
           lbltitle1=UILabel(frame: CGRect(x: 10, y:lbltitle.frame.maxY, width: viewshare.frame.size.width-20, height: 40))
        }
        else
        {
          lbltitle1=UILabel(frame: CGRect(x: 10, y:lbltitle.frame.maxY, width: viewshare.frame.size.width-20, height: 30))
        }
        
        lbltitle1.text="This will remove the listing from your favourites"
        lbltitle1.textColor=UIColor.black.withAlphaComponent(0.7)
        lbltitle1.font=UIFont.systemFont(ofSize: 13)
        lbltitle1.textAlignment = .left
        lbltitle1.numberOfLines=3
        viewshare.addSubview(lbltitle1)

        let lbltitle2=UILabel(frame: CGRect(x: 10, y:lbltitle1.frame.maxY, width: viewshare.frame.size.width-20, height: 20))
        lbltitle2.text="Don't show this message again"
        lbltitle2.textColor=UIColor.black.withAlphaComponent(0.7)
        lbltitle2.font=UIFont.systemFont(ofSize: 13)
        lbltitle2.textAlignment = .left
        lbltitle2.numberOfLines=3
        viewshare.addSubview(lbltitle2)
        
        let imgdot=UIImageView(frame: CGRect(x:viewshare.frame.size.width-40, y:lbltitle1.frame.maxY, width: 20, height: 20))
        imgdot.image=UIImage(named:"ic_dot1")
        viewshare.addSubview(imgdot)

        let middleview = UIView(frame: CGRect(x:0, y:viewshare.frame.size.height-36 ,width:viewshare.frame.size.width, height:0.6))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewshare.addSubview(middleview)
        
        
        let btncancle = UIButton(frame:CGRect(x:5, y:middleview.frame.maxY, width:viewshare.frame.size.width/2-20, height:30))
        btncancle.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btncancle.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btncancle.addTarget(self, action:#selector(btncancelclick), for: UIControlEvents.touchUpInside)
        btncancle.setTitle("Cancle", for: UIControlState.normal)
        viewshare.addSubview(btncancle)
        
        
        let btnApply = UIButton(frame:CGRect(x:btncancle.frame.maxX+25 , y:middleview.frame.maxY, width:viewshare.frame.size.width/2-10, height:30))
        btnApply.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnApply.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnApply.addTarget(self, action:#selector(btncancelclick), for: UIControlEvents.touchUpInside)
        btnApply.setTitle("Remove", for: UIControlState.normal)
        viewshare.addSubview(btnApply)
        
        let lineview = UIView(frame: CGRect(x:btncancle.frame.maxX+8, y:viewshare.frame.size.height-36,width:0.6, height:36))
        lineview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewshare.addSubview(lineview)
        
    }
   
    func loadTableviewlist()
    {
        tbl_listview1 = UITableView()
        tbl_listview1.frame = CGRect(x: 0,y:lbl_category.frame.maxY+5,width: appDelegate.screenWidth,height:appDelegate.screenHeight-70)
        //  tbl_listview1.backgroundColor=UIColor.white
        tbl_listview1.delegate = self
        tbl_listview1.dataSource = self
        
        tbl_listview1.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tbl_listview1)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_listview1.tableFooterView = footerView
        tbl_listview1.tableFooterView?.isHidden = true
        tbl_listview1.backgroundColor = UIColor.clear
        let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
        tbl_listview1.addGestureRecognizer(tapGestureRecognizer1)

    }
    func loadBottomCollectionview()
    {
        viewexpand=UIView(frame: CGRect(x: 0, y: appDelegate.screenHeight-80, width: appDelegate.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if appDelegate.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)

        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if appDelegate.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if appDelegate.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-80, width: appDelegate.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-210, width: appDelegate.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==5
        {
            let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
            navigationController?.pushViewController(agentView, animated: true)
        }
        else if indexPath.row==7
        {
            let suggestedView = SuggestedProperties(nibName: "SuggestedProperties", bundle: nil)
            navigationController?.pushViewController(suggestedView, animated: true)
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==8
        {
            let settingView = SettingsView(nibName: "SettingsView", bundle: nil)
            navigationController?.pushViewController(settingView, animated: true)
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }



    }
    
    //UITableview Delegate and DataSource methods
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return 10
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyTestCell")
        cell.backgroundColor=UIColor.clear
        tbl_listview1.separatorColor=UIColor.clear
        
        let viewback1=UIView(frame: CGRect(x: 16, y: 10, width: appDelegate.screenWidth-32, height: appDelegate.screenWidth-66))
        viewback1.backgroundColor=UIColor.white
        viewback1.layer.cornerRadius=16
        cell.addSubview(viewback1)
        
        viewback1.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: viewback1.bounds, cornerRadius: cornerRadius)
        viewback1.layer.masksToBounds = false
        viewback1.layer.shadowColor = shadowColor?.cgColor
        viewback1.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewback1.layer.shadowOpacity = shadowOpacity
        viewback1.layer.shadowPath = shadowPath.cgPath
        viewback1.isUserInteractionEnabled=false
        
        let viewback=UIView(frame: CGRect(x: 15, y: 5, width: appDelegate.screenWidth-30, height: appDelegate.screenWidth-60))
        viewback.backgroundColor=UIColor.white
        viewback.layer.cornerRadius=16
        cell.addSubview(viewback)
        
        let lblprice=UILabel(frame: CGRect(x: viewback.frame.size.width-120, y: 0, width: 100, height: 30))
        lblprice.text="$512,111"
        lblprice.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblprice.font=UIFont.boldSystemFont(ofSize: 20)
        lblprice.textAlignment = .right
        viewback.addSubview(lblprice)
        
        let lblkm=UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: 30))
        lblkm.text="1.2 km"
        lblkm.textColor=UIColor.black.withAlphaComponent(0.7)
        lblkm.font=UIFont.systemFont(ofSize: 13)
        lblkm.textAlignment = .left
        viewback.addSubview(lblkm)
        
        let img_home=UIImageView(frame: CGRect(x:5, y: 30, width: viewback.frame.size.width-10, height: appDelegate.screenWidth-140))
        img_home.image=UIImage(named: "home")
        img_home.clipsToBounds=true
        viewback.addSubview(img_home)
        
        let lbladd=UILabel(frame: CGRect(x: 10, y:img_home.frame.maxY, width: viewback.frame.size.width-20, height: 25))
        lbladd.text="123 Fariview Park Trial, Calgary AB, T6F G9Y"
        lbladd.textColor=UIColor.black.withAlphaComponent(0.7)
        lbladd.font=UIFont.systemFont(ofSize: 13)
        lbladd.textAlignment = .left
        viewback.addSubview(lbladd)
        
        let btn_map=UIButton(frame: CGRect(x: 10, y: lbladd.frame.maxY+2, width: 14, height: 15))
        btn_map.setBackgroundImage(UIImage(named:"ic_car_directions"), for: .normal)
     //   btn_map.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        viewback.addSubview(btn_map)
        
        let btn_mapview=UIButton(frame: CGRect(x:btn_map.frame.maxX, y: lbladd.frame.maxY, width: 60, height: 20))
        btn_mapview.setTitle("Directions", for: .normal)
        btn_mapview.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
        btn_mapview.titleLabel?.font=UIFont.systemFont(ofSize: 11)
      //  btn_mapview.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        viewback.addSubview(btn_mapview)
        
        let viewnew=UIView(frame: CGRect(x: UIScreen.main.bounds.size.width/100*104, y: viewback.frame.size.height/2-50, width: 90, height: 100))
        viewnew.backgroundColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        viewnew.layer.cornerRadius=7
        cell.addSubview(viewnew)
        
        let btn_remove_img=UIButton(frame: CGRect(x: 22.5, y: 20, width: 30, height: 30))
        btn_remove_img.setBackgroundImage(UIImage(named:"ic_swipe_delete"), for: .normal)
       // btn_remove_img.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        viewnew.addSubview(btn_remove_img)
        
        let btn_remove=UIButton(frame: CGRect(x:0, y: 60, width:75, height: 30))
        btn_remove.setTitle("Remove", for: .normal)
        btn_remove.setTitleColor(UIColor.white, for: .normal)
        btn_remove.titleLabel?.font=UIFont.systemFont(ofSize: 15)
       // btn_remove.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        viewnew.addSubview(btn_remove)
        
        let viewnew1=UIView(frame: CGRect(x:-128, y:viewback.frame.size.height/2-50, width:110, height: 100))
        viewnew1.backgroundColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        viewnew1.layer.cornerRadius=7
        cell.addSubview(viewnew1)
        
        let btn_remove_img1=UIButton(frame: CGRect(x: viewnew1.frame.size.width/2-16, y: 20, width: 40, height: 40))
        btn_remove_img1.setBackgroundImage(UIImage(named:"ic_swipe_showing"), for: .normal)
        // btn_remove_img.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        viewnew1.addSubview(btn_remove_img1)
        
        let btn_remove1=UIButton(frame: CGRect(x:0, y: 60, width:110, height: 30))
        btn_remove1.setTitle("Request Showing", for: .normal)
        btn_remove1.setTitleColor(UIColor.white, for: .normal)
        btn_remove1.titleLabel?.font=UIFont.systemFont(ofSize: 11)
        // btn_remove.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        viewnew1.addSubview(btn_remove1)
       // cell.isUserInteractionEnabled=false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return appDelegate.screenWidth-50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if cellleft==true
        {
            cellleft=false
            tbl_listview1.reloadData()
        }

    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        if cellleft==true
        {
            cellleft=false
            
            tbl_listview1.reloadData()
        }
        else
        {

        if tableView==tbl_listview1
        {
            let more = UITableViewRowAction(style: .normal, title: "             ") { action, index in
                
                self.loadRemovePopup()
               // tableView.setEditing(true, animated: false)
            }
            
             more.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
            return [more]
        }
        else
        {
            return nil
        }
        }
         return nil
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView==tbl_listview1
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    
    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }
    func imageTapped2(tapGestureRecognizer: UISwipeGestureRecognizer) {
        
        cellleft=true
        tbl_listview1.reloadData()
        if tapGestureRecognizer.state == UIGestureRecognizerState.ended {
            let swipeLocation = tapGestureRecognizer.location(in: self.tbl_listview1)
            if let swipedIndexPath = tbl_listview1.indexPathForRow(at: swipeLocation) {
                if let swipedCell = self.tbl_listview1.cellForRow(at: swipedIndexPath) {
                    //let cell = tbl_listview1.cellForRow(at: swipedIndexPath);
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        swipedCell.frame = CGRect(x: swipedCell.frame.origin.x + 120, y: swipedCell.frame.origin.y, width: swipedCell.bounds.size.width - 120, height: swipedCell.bounds.size.height)
                        
                    })
                    
                    let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped3(tapGestureRecognizer:)))
                    tapGestureRecognizer1.direction = UISwipeGestureRecognizerDirection.left
                    swipedCell.addGestureRecognizer(tapGestureRecognizer1)
                }
            }
        }
    }
    
    func imageTapped3(tapGestureRecognizer: UISwipeGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: {
            self.cellleft=false
            self.tbl_listview1.reloadData()
            
        }, completion: { (finished) in
        })
        
    }
    
    func btncancelclick(sender:UIButton)
    {
        viewbackground.removeFromSuperview()
        viewshare.removeFromSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
