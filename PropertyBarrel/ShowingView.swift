//
//  ShowingView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/12/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class ShowingView: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate {
    var appDelegate:AppDelegate!
    var viewtop:UIView!
    var viewexpand:UIView!
    var viewbackground:UIView!
     var viewbackground_suggest:UIView!
    var viewshare:UIView!
    var popview:UIView!
    var viewSuggest:UIView!
    
    var img_Backgrund:UIImageView!
    var lbl_category_name:UILabel!
    var lbl_category_name1:UILabel!
    var lbl_category:UILabel!
    var txtSearch:UITextField!
    
    var tbl_listview1=UITableView()
    var tbl_suggest=UITableView()
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selectedlist:Bool!
    var arr_category_img:[String] = ["ic_search","showing1","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Showing","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Showing","Messaging","Menu","Mortgage Calculator","Contacts","Map","Suggested Properties","Settings","Share App"]
    var selected:Bool!
    
    @IBInspectable var cornerRadius: CGFloat = 16
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = Int(1.1)
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    var cellleft:Bool!
    var selectsuggest:Bool!
    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        selected=false
        selectedlist=false
        selectsuggest=false
        loadInitialUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
    }
    
    func loadInitialUI()
    {
        //        img_Backgrund=UIImageView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        //        img_Backgrund.image=UIImage(named: "im_bg")
        //        img_Backgrund.clipsToBounds=true
        //        img_Backgrund.alpha=0.2
        //        self.view.addSubview(img_Backgrund)
        self.view.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        viewtop=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: 45))
        viewtop.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.view.addSubview(viewtop)
        
        lbl_category_name1=UILabel(frame: CGRect(x: 0, y:20, width: appDelegate.screenWidth, height: 25))
        lbl_category_name1.text="Showings"
        lbl_category_name1.textColor=UIColor.gray
        lbl_category_name1.font=UIFont.boldSystemFont(ofSize: 16)
        lbl_category_name1.textAlignment = .center
        viewtop.addSubview(lbl_category_name1)
        
        let btn_ref=UIButton(frame: CGRect(x:appDelegate.screenWidth-35, y: 18, width: 25, height: 25))
        btn_ref.setBackgroundImage(UIImage(named:"showingadd"), for: .normal)
        btn_ref.addTarget(self, action: #selector(self.btn_add_action), for: .touchUpInside)
        viewtop.addSubview(btn_ref)
        
        let btn_ref1=UIButton(frame: CGRect(x:appDelegate.screenWidth-45, y: 8, width: 45, height: 45))
        btn_ref1.addTarget(self, action: #selector(self.btn_add_action), for: .touchUpInside)
        viewtop.addSubview(btn_ref1)

        
//        let imgremove=UIImageView(frame: CGRect(x: 15, y: viewtop.frame.maxY+5, width: 20, height: 20))
//        imgremove.image=UIImage(named: "deletedark")
//        self.view.addSubview(imgremove)
//        
//        let imgshowing=UIImageView(frame: CGRect(x: appDelegate.screenWidth-35, y: viewtop.frame.maxY+5, width: 20, height: 20))
//        imgshowing.image=UIImage(named: "ic_dettop_showing")
//        self.view.addSubview(imgshowing)
//        
//        let imgarrow=UIImageView(frame: CGRect(x: imgremove.frame.maxX+5, y: viewtop.frame.maxY+10, width: 50, height: 10))
//        imgarrow.image=UIImage(named: "leftarrow")
//        self.view.addSubview(imgarrow)
//        
//        lbl_category=UILabel(frame: CGRect(x: appDelegate.screenWidth/2-100, y:viewtop.frame.maxY+2, width: 200, height: 25))
//        lbl_category.text="Request Showing or Remove"
//        lbl_category.textColor=UIColor.gray
//        lbl_category.font=UIFont.systemFont(ofSize: 11)
//        lbl_category.textAlignment = .center
//        self.view.addSubview(lbl_category)
//        
//        let imgarrow1=UIImageView(frame: CGRect(x: appDelegate.screenWidth-90, y: viewtop.frame.maxY+10, width: 50, height: 10))
//        imgarrow1.image=UIImage(named: "rightarrow")
//        self.view.addSubview(imgarrow1)
        
        loadTableviewlist()
        loadBottomCollectionview()
    }
    
    func loadaddShowingUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewbackground)
        
        popview = UIView(frame: CGRect(x:15, y:40, width:appDelegate.screenWidth-30, height:350))
        popview.backgroundColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        popview.layer.cornerRadius = 15.0
        self.view.addSubview(popview)
        
        let lblbooking = UILabel(frame: CGRect(x: 0, y: 5, width:popview.frame.size.width, height: 25))
        lblbooking.text = "Add Showing"
        lblbooking.font = UIFont.boldSystemFont(ofSize:18)
        lblbooking.textAlignment = .center
        lblbooking.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        popview.addSubview(lblbooking)
        
        let lblpopadd = UILabel(frame:CGRect(x:15, y:lblbooking.frame.maxY+5, width:80, height: 20))
        lblpopadd.text = "Address:"
        lblpopadd.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblpopadd.font = UIFont.boldSystemFont(ofSize:15)
        lblpopadd.textAlignment = .left
        popview.addSubview(lblpopadd)
        
        let lblid = UILabel(frame:CGRect(x: 15, y:lblpopadd.frame.maxY, width:105, height: 20))
        lblid.text = "ID-R2134645"
        lblid.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblid.font = UIFont.boldSystemFont(ofSize:12)
        lblid.textAlignment = .left
        popview.addSubview(lblid)
        
        let lblwest = UILabel(frame:CGRect(x:popview.frame.size.width-195, y:lblpopadd.frame.maxY, width:195, height: 20))
        lblwest.text = "24 West Anderson Drive"
        lblwest.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblwest.font = UIFont.boldSystemFont(ofSize:12)
        lblwest.textAlignment = .center
        popview.addSubview(lblwest)
        
        let topview = UIView(frame: CGRect(x:15, y:lblid.frame.maxY+2,width:popview.frame.size.width-30, height:1))
        topview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(topview)
        
        let lblclient = UILabel(frame:CGRect(x: 15, y:topview.frame.maxY+5, width:95, height: 20))
        lblclient.text = "Client Name"
        lblclient.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblclient.font = UIFont.boldSystemFont(ofSize:15)
        lblclient.textAlignment = .left
        popview.addSubview(lblclient)
        
        let lblfred = UILabel(frame:CGRect(x: 15, y:lblclient.frame.maxY, width:200, height: 20))
        lblfred.text = "Fredrick Fredicson"
        lblfred.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblfred.font = UIFont.boldSystemFont(ofSize:15)
        lblfred.textAlignment = .left
        popview.addSubview(lblfred)
        
        let btbfred=UIButton(frame:CGRect(x: 15, y:lblclient.frame.maxY, width:200, height: 20))
        btbfred.addTarget(self, action:#selector(btbfredapply), for: UIControlEvents.touchUpInside)
        popview.addSubview(btbfred)
        
       let imageprofile = UIImageView(frame: CGRect(x:popview.frame.size.width-65, y:topview.frame.maxY+3, width:50, height:50))
        imageprofile.image = UIImage(named: "profile-1")
        popview.addSubview(imageprofile)
        
        let topview2 = UIView(frame: CGRect(x:15, y:imageprofile.frame.maxY,width:popview.frame.size.width-30, height:1))
        topview2.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(topview2)
        
        let lbltime = UILabel(frame:CGRect(x:10, y:topview2.frame.maxY+10, width:120, height: 20))
        lbltime.text = "Showing Time:"
        lbltime.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lbltime.font = UIFont.boldSystemFont(ofSize:15)
        lbltime.textAlignment = .left
        popview.addSubview(lbltime)
        
        let lblform = UILabel(frame:CGRect(x: 5, y:lbltime.frame.maxY+10, width:popview.frame.size.width/2-5, height: 20))
        lblform.text = "From"
        lblform.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblform.font = UIFont.boldSystemFont(ofSize:15)
        lblform.textAlignment = .center
        popview.addSubview(lblform)
        
        let lblto = UILabel(frame:CGRect(x:popview.frame.size.width-130, y:lbltime.frame.maxY+10, width:popview.frame.size.width/2-10, height: 20))
        lblto.textColor = UIColor.init(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0)
        lblto.text = "To"
        lblto.font = UIFont.boldSystemFont(ofSize:15)
        lblto.textAlignment = .center
        popview.addSubview(lblto)
        
        let Picker  = UIDatePicker(frame: CGRect(x:5,y: lblform.frame.maxY+8,width:popview.frame.size.width/2-5,height:100))
        Picker.backgroundColor = UIColor.white
        Picker.datePickerMode = UIDatePickerMode.time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        Picker.addTarget(self, action: #selector(self.onDatePickerValueChanged), for: UIControlEvents.valueChanged)
        popview.addSubview(Picker)
        
        let DatePicker = UIDatePicker(frame:CGRect(x:Picker.frame.maxX+5,y: lblform.frame.maxY+8,width:popview.frame.size.width/2-10,height:100))
        DatePicker.datePickerMode = UIDatePickerMode.time
        DatePicker.backgroundColor = UIColor.white
        DatePicker.addTarget(self, action: #selector(self.onDatePickerValueChanged), for: UIControlEvents.valueChanged)
        popview.addSubview(DatePicker)
        
        let middleview = UIView(frame: CGRect(x:0, y:popview.frame.size.height-36,width:popview.frame.size.width, height:0.6))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(middleview)
        
      let  btncancle = UIButton(frame:CGRect(x:5, y:middleview.frame.maxY, width:popview.frame.size.width/2-20, height:35))
        btncancle.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btncancle.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btncancle.addTarget(self, action:#selector(btnclickapply), for: UIControlEvents.touchUpInside)
        btncancle.setTitle("Cancle", for: UIControlState.normal)
        popview.addSubview(btncancle)
        
      let  btnApply = UIButton(frame:CGRect(x:btncancle.frame.maxX+25 , y:middleview.frame.maxY, width:popview.frame.size.width/2-10, height:35))
        btnApply.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnApply.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btnApply.addTarget(self, action:#selector(btnclickapply), for: UIControlEvents.touchUpInside)
        btnApply.setTitle("Apply", for: UIControlState.normal)
        popview.addSubview(btnApply)
        
        let lineview = UIView(frame: CGRect(x:btncancle.frame.maxX+8, y:popview.frame.size.height-36,width:0.6, height:36))
        lineview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lineview)
        
    }
    
    func onDatePickerValueChanged(datePicker: UIDatePicker) {
        let date1 = dateFormatter.string(from: datePicker.date)
        print(date1)
    }
    
    func loadsuggestPopupUI()
    {
        viewbackground_suggest=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground_suggest.backgroundColor=UIColor.black.withAlphaComponent(0.5)
       // let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
      //  viewbackground.addGestureRecognizer(tapGestureRecognizer)
        //viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground_suggest)
        
        viewSuggest=UIView(frame: CGRect(x: 15, y:65, width: appDelegate.screenWidth-30, height: appDelegate.screenHeight-120))
        viewSuggest.backgroundColor=UIColor.white
        viewSuggest.layer.cornerRadius=14
        self.view.addSubview(viewSuggest)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewSuggest.frame.size.width, height: 30))
        lbltitle.text="Select Client"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewSuggest.addSubview(lbltitle)
        
        let viewback2=UIView(frame: CGRect(x: 16, y: lbltitle.frame.maxY+6, width: viewSuggest.frame.size.width-32, height: 28))
        viewback2.backgroundColor=UIColor.white
        viewSuggest.addSubview(viewback2)
        
        viewback2.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: viewback2.bounds, cornerRadius: 8)
        
        viewback2.layer.masksToBounds = false
        viewback2.layer.shadowColor = shadowColor?.cgColor
        viewback2.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewback2.layer.shadowOpacity = shadowOpacity
        viewback2.layer.shadowPath = shadowPath.cgPath
        
        txtSearch=UITextField(frame: CGRect(x: 15, y: lbltitle.frame.maxY+5, width: viewSuggest.frame.size.width-30, height: 30))
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search", attributes:[NSForegroundColorAttributeName: UIColor.black.withAlphaComponent(0.6),NSFontAttributeName :UIFont(name: "Arial", size: 16)!])
        txtSearch.textColor=UIColor.black.withAlphaComponent(0.8)
        txtSearch.font=UIFont.systemFont(ofSize: 16)
        txtSearch.textAlignment = .center
        txtSearch.layer.cornerRadius=8
        txtSearch.backgroundColor=UIColor.white
        txtSearch.delegate=self
        viewSuggest.addSubview(txtSearch)
        
        let btnsarch=UIButton(frame: CGRect(x: 3, y: 5, width: 20, height: 20))
        btnsarch.setBackgroundImage(UIImage(named:"ic_agent_search"), for: .normal)
        txtSearch.addSubview(btnsarch)
        
        tbl_suggest = UITableView()
        tbl_suggest.frame = CGRect(x: 0,y:txtSearch.frame.maxY+5,width: viewSuggest.frame.size.width,height:viewSuggest.frame.size.height-108)
        //  tbl_listview1.backgroundColor=UIColor.white
        tbl_suggest.delegate = self
        tbl_suggest.dataSource = self
        
        tbl_suggest.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        viewSuggest.addSubview(tbl_suggest)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_suggest.tableFooterView = footerView
        tbl_suggest.tableFooterView?.isHidden = true
        tbl_suggest.backgroundColor = UIColor.clear
        
        let viewnew2=UIView(frame: CGRect(x:0, y: viewSuggest.frame.size.height-38, width:viewSuggest.frame.size.width, height: 0.7))
        viewnew2.backgroundColor=UIColor.gray
        viewSuggest.addSubview(viewnew2)
        
        let btncancel=UIButton(frame: CGRect(x: 0, y:viewnew2.frame.maxY, width: viewSuggest.frame.size.width/2-1, height: 38))
        btncancel.setTitle("Cancel", for: .normal)
        btncancel.setTitleColor(UIColor.gray, for: .normal)
        btncancel.titleLabel?.font=UIFont.systemFont(ofSize: 16)
        btncancel.addTarget(self, action: #selector(self.btncancel_action1), for: .touchUpInside)
        viewSuggest.addSubview(btncancel)
        
        let viewnew1=UIView(frame: CGRect(x:btncancel.frame.maxX, y: viewnew2.frame.maxY, width: 0.7, height: 38))
        viewnew1.backgroundColor=UIColor.gray
        viewSuggest.addSubview(viewnew1)
        
        let btnapply=UIButton(frame: CGRect(x:viewnew1.frame.maxX, y: viewnew2.frame.maxY, width: viewSuggest.frame.size.width/2, height: 38))
        btnapply.setTitle("Select", for: .normal)
        btnapply.setTitleColor(UIColor.gray, for: .normal)
        btnapply.titleLabel?.font=UIFont.systemFont(ofSize: 16)
        btnapply.addTarget(self, action: #selector(self.btncancel_action1), for: .touchUpInside)
        viewSuggest.addSubview(btnapply)
    }

    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-80, width: appDelegate.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
        
    }
    
    
    func loadTableviewlist()
    {
        tbl_listview1 = UITableView()
        tbl_listview1.frame = CGRect(x: 0,y:viewtop.frame.maxY+5,width: appDelegate.screenWidth,height:appDelegate.screenHeight-70)
        //  tbl_listview1.backgroundColor=UIColor.white
        tbl_listview1.delegate = self
        tbl_listview1.dataSource = self
        
        tbl_listview1.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tbl_listview1)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_listview1.tableFooterView = footerView
        tbl_listview1.tableFooterView?.isHidden = true
        tbl_listview1.backgroundColor = UIColor.clear
        //let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
      //  tbl_listview1.addGestureRecognizer(tapGestureRecognizer1)
    }
    
    func loadBottomCollectionview()
    {
        viewexpand=UIView(frame: CGRect(x: 0, y: appDelegate.screenHeight-80, width: appDelegate.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if appDelegate.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if appDelegate.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if appDelegate.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-80, width: appDelegate.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-210, width: appDelegate.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==5
        {
            if appDelegate.selectuser==true
            {
                let contactView = ContactsView(nibName: "ContactsView", bundle: nil)
                navigationController?.pushViewController(contactView, animated: true)
            }
            else
            {
                let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
                navigationController?.pushViewController(agentView, animated: true)
            }
            
        }
        else if indexPath.row==7
        {
            let suggestedView = SuggestedProperties(nibName: "SuggestedProperties", bundle: nil)
            navigationController?.pushViewController(suggestedView, animated: true)
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==8
        {
            let settingView = SettingsView(nibName: "SettingsView", bundle: nil)
            navigationController?.pushViewController(settingView, animated: true)
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }
        
        
        
    }
    
    //UITableview Delegate and DataSource methods
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if selectsuggest==true
        {
            return 10
        }
        else
        {
            return 10
        }
        
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyTestCell")
        
        if selectsuggest==true
        {
            let img_home=UIImageView(frame: CGRect(x:10, y: 5, width: 50, height: 50))
            img_home.image=UIImage(named: "agent_pic")
            img_home.layer.cornerRadius=8
            img_home.clipsToBounds=true
            cell.addSubview(img_home)
            
            let lblname=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y:3, width: 200, height: 20))
            lblname.text="Ashley Jenkins"
            lblname.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            lblname.font=UIFont.systemFont(ofSize: 14)
            lblname.textAlignment = .left
            cell.addSubview(lblname)
            
            let lbladd=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y:lblname.frame.maxY-3, width: 120, height: 20))
            lbladd.text="1-44-444442154"
            lbladd.textColor=UIColor.black.withAlphaComponent(0.7)
            lbladd.font=UIFont.systemFont(ofSize: 11)
            lbladd.textAlignment = .left
            cell.addSubview(lbladd)
            
            let lbladds=UILabel(frame: CGRect(x: lbladd.frame.maxX+5, y:lblname.frame.maxY-3, width: 100, height: 20))
            lbladds.text="babs@gmail.com"
            lbladds.textColor=UIColor.black.withAlphaComponent(0.7)
            lbladds.font=UIFont.systemFont(ofSize: 11)
            //lbladds.numberOfLines=3
            lbladds.textAlignment = .left
            cell.addSubview(lbladds)
            
//            if arrselected.contains(indexPath.row)
//            {
//                cell.backgroundColor=UIColor.gray.withAlphaComponent(0.4)
//            }
  
        }
        else
        {
        cell.backgroundColor=UIColor.clear
        tbl_listview1.separatorColor=UIColor.clear
        
        let viewback1=UIView(frame: CGRect(x: 16, y: 10, width: appDelegate.screenWidth-32, height: appDelegate.screenWidth-66))
        viewback1.backgroundColor=UIColor.white
        viewback1.layer.cornerRadius=16
        cell.addSubview(viewback1)
        
        viewback1.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: viewback1.bounds, cornerRadius: cornerRadius)
        viewback1.layer.masksToBounds = false
        viewback1.layer.shadowColor = shadowColor?.cgColor
        viewback1.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewback1.layer.shadowOpacity = shadowOpacity
        viewback1.layer.shadowPath = shadowPath.cgPath
        
        let viewback=UIView(frame: CGRect(x: 15, y: 5, width: appDelegate.screenWidth-30, height: appDelegate.screenWidth-60))
        viewback.backgroundColor=UIColor.white
        if indexPath.row==0
        {
          viewback.layer.borderColor = UIColor.init(colorLiteralRed: 58.0/255.0, green: 181.0/255.0, blue: 75.0/255.0, alpha: 1.0).cgColor
            viewback.layer.borderWidth=2.0
        }
        viewback.layer.cornerRadius=16
        cell.addSubview(viewback)
        
        let lblprice=UILabel(frame: CGRect(x: viewback.frame.size.width-130, y: 0, width: 120, height: 30))
        lblprice.text="1:00 pm to 2:00 pm"
        //lblprice.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblprice.textColor=UIColor.black.withAlphaComponent(0.7)
        lblprice.font=UIFont.systemFont(ofSize: 12)
        lblprice.textAlignment = .right
        viewback.addSubview(lblprice)
        
        let lblkm=UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: 30))
        lblkm.text="july 15 2017"
        lblkm.textColor=UIColor.black.withAlphaComponent(0.7)
        lblkm.font=UIFont.systemFont(ofSize: 12)
        lblkm.textAlignment = .left
        viewback.addSubview(lblkm)
        
        let img_home=UIImageView(frame: CGRect(x:5, y: 30, width: viewback.frame.size.width-10, height: appDelegate.screenWidth-140))
        img_home.image=UIImage(named: "home")
        img_home.clipsToBounds=true
        viewback.addSubview(img_home)
        
        let lbladd=UILabel(frame: CGRect(x: 10, y:img_home.frame.maxY, width: viewback.frame.size.width-20, height: 25))
        lbladd.text="123 Fariview Park Trial, Calgary AB, T6F G9Y"
        lbladd.textColor=UIColor.black.withAlphaComponent(0.7)
        lbladd.font=UIFont.systemFont(ofSize: 13)
        lbladd.textAlignment = .left
        viewback.addSubview(lbladd)
        
        let lblname=UILabel(frame: CGRect(x: 10, y:lbladd.frame.maxY, width: viewback.frame.size.width-20, height: 25))
        lblname.text="Carter and Christine Parker"
        lblname.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblname.font=UIFont.systemFont(ofSize: 13)
        lblname.textAlignment = .left
        viewback.addSubview(lblname)
            
            cell.isUserInteractionEnabled=false
        }
//        let btn_map=UIButton(frame: CGRect(x: 10, y: lbladd.frame.maxY+2, width: 14, height: 15))
//        btn_map.setBackgroundImage(UIImage(named:"img_map_pointer"), for: .normal)
//        //   btn_map.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
//        viewback.addSubview(btn_map)
//        
//        let btn_mapview=UIButton(frame: CGRect(x:btn_map.frame.maxX, y: lbladd.frame.maxY, width: 60, height: 20))
//        btn_mapview.setTitle("Directions", for: .normal)
//        btn_mapview.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
//        btn_mapview.titleLabel?.font=UIFont.systemFont(ofSize: 11)
//        //  btn_mapview.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
//        viewback.addSubview(btn_mapview)
        
//        let viewnew=UIView(frame: CGRect(x: UIScreen.main.bounds.size.width/100*104, y: viewback.frame.size.height/2-50, width: 90, height: 100))
//        viewnew.backgroundColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
//        viewnew.layer.cornerRadius=7
//        cell.addSubview(viewnew)
//        
//        let btn_remove_img=UIButton(frame: CGRect(x: 22.5, y: 20, width: 30, height: 30))
//        btn_remove_img.setBackgroundImage(UIImage(named:"ic_swipe_delete"), for: .normal)
//        // btn_remove_img.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
//        viewnew.addSubview(btn_remove_img)
//        
//        let btn_remove=UIButton(frame: CGRect(x:0, y: 60, width:75, height: 30))
//        btn_remove.setTitle("Remove", for: .normal)
//        btn_remove.setTitleColor(UIColor.white, for: .normal)
//        btn_remove.titleLabel?.font=UIFont.systemFont(ofSize: 15)
//        // btn_remove.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
//        viewnew.addSubview(btn_remove)
//        
//        let viewnew1=UIView(frame: CGRect(x:-128, y:viewback.frame.size.height/2-50, width:110, height: 100))
//        viewnew1.backgroundColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
//        viewnew1.layer.cornerRadius=7
//        cell.addSubview(viewnew1)
//        
//        let btn_remove_img1=UIButton(frame: CGRect(x: viewnew1.frame.size.width/2-16, y: 20, width: 40, height: 40))
//        btn_remove_img1.setBackgroundImage(UIImage(named:"ic_swipe_showing"), for: .normal)
//        // btn_remove_img.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
//        viewnew1.addSubview(btn_remove_img1)
//        
//        let btn_remove1=UIButton(frame: CGRect(x:0, y: 60, width:110, height: 30))
//        btn_remove1.setTitle("Request Showing", for: .normal)
//        btn_remove1.setTitleColor(UIColor.white, for: .normal)
//        btn_remove1.titleLabel?.font=UIFont.systemFont(ofSize: 11)
//        // btn_remove.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
//        viewnew1.addSubview(btn_remove1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectsuggest==true
        {
            return 60
        }
        else
        {
            return appDelegate.screenWidth-50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        if cellleft==true
//        {
//            cellleft=false
//            tbl_listview1.reloadData()
//        }
        
    }
    
/*    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        if cellleft==true
        {
            cellleft=false
            
            tbl_listview1.reloadData()
        }
        else
        {
            
            if tableView==tbl_listview1
            {
                let more = UITableViewRowAction(style: .normal, title: "             ") { action, index in
                    
                    // tableView.setEditing(true, animated: false)
                }
                
                more.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
                return [more]
            }
            else
            {
                return nil
            }
        }
        return nil
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView==tbl_listview1
        {
            return true
        }
        else
        {
            return false
        }
        
    }*/
    
    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }
/*    func imageTapped2(tapGestureRecognizer: UISwipeGestureRecognizer) {
        
        cellleft=true
        tbl_listview1.reloadData()
        if tapGestureRecognizer.state == UIGestureRecognizerState.ended {
            let swipeLocation = tapGestureRecognizer.location(in: self.tbl_listview1)
            if let swipedIndexPath = tbl_listview1.indexPathForRow(at: swipeLocation) {
                if let swipedCell = self.tbl_listview1.cellForRow(at: swipedIndexPath) {
                    //let cell = tbl_listview1.cellForRow(at: swipedIndexPath);
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        swipedCell.frame = CGRect(x: swipedCell.frame.origin.x + 120, y: swipedCell.frame.origin.y, width: swipedCell.bounds.size.width - 120, height: swipedCell.bounds.size.height)
                        
                    })
                    
                    let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped3(tapGestureRecognizer:)))
                    tapGestureRecognizer1.direction = UISwipeGestureRecognizerDirection.left
                    swipedCell.addGestureRecognizer(tapGestureRecognizer1)
                }
            }
        }
    }
    
    func imageTapped3(tapGestureRecognizer: UISwipeGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: {
            self.cellleft=false
            self.tbl_listview1.reloadData()
            
        }, completion: { (finished) in
        })
        
    }*/
    
    func btn_add_action(sender:UIButton)
    {
        loadaddShowingUI()
    }
    
    func btnclickapply(sender:UIButton)
    {
        viewbackground.removeFromSuperview()
        popview.removeFromSuperview()
    }
    
    func btbfredapply()
    {
        selectsuggest=true
        loadsuggestPopupUI()
    }

    func btncancel_action1()
    {
        selectsuggest=false
        viewbackground_suggest.removeFromSuperview()
        viewSuggest.removeFromSuperview()
        tbl_suggest.removeFromSuperview()
    }
    
    //UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
