//
//  PropertySearch.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/3/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit
@IBDesignable

class PropertySearch: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
  
    var appDelegate:AppDelegate!
    var viewtop:UIView!
    var viewexpand:UIView!
    var viewslide=UIView()
    var viewbackground:UIView!
    var viewbackground1:UIView!
    var viewProperty:UIView!
    var viewshare=UIView()
    var viewSuggest=UIView()
    var viewnew1=UIView()
    var viewback=UIView()
    var viewback1=UIView()

    var img_Backgrund:UIImageView!
    var lbl_category_name:UILabel!
    
    var lbl_category=UILabel()
    var imgarrleft=UIImageView()
    var imgarrright=UIImageView()
    var imgremove=UIImageView()
    var imgshowing=UIImageView()
    
    var btn_filter=UIButton()
    var btn_filter1=UIButton()
    var btnSearch=UIButton()
    
    var txtSearch:UITextField!
    
    var tbl_listview:UITableView!
    var tbl_listview1=UITableView()
    var tbl_filter=UITableView()
    var tbl_property=UITableView()
    var tbl_suggest=UITableView()
    
    var pickerfrom=UIPickerView()
    var pickerto=UIPickerView()

    
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    
    var selected:Bool!
    var selectedlist:Bool!
    var selectedfilter:Bool!
    var selectedproperty:Bool!
    var selectsuggest:Bool!
    
    var arrselected:NSMutableArray=[]
    
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]

    var arr_filter_name:[String] = ["Property Type","Price","Beds","Bath","Open House","Condo Fees","Status","Area"]
    
    var arr_property_type:[String] = ["Single-Family Home","Condo / Townhouse","Rental","Lots","Mobile Homes","Multi-Family","Commercial","Industrial"]
    var arrpricee:NSMutableArray=[]
    var arr_price:[String] = ["$101,000","$102,000","$103,000","$104,000","$105,000","$106,000","$107,000","$108,000","$109,000","$110,000","$111,000","$112,000","$113,000","$114,000","$115,000","$116,000"]
    var inx:NSInteger!
    var inx1:NSInteger!
    var inx2:NSInteger!
    var strvalue=""
    var strvalue1=""
    @IBInspectable var cornerRadius: CGFloat = 16
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = Int(1.1)
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    var cellleft:Bool!
    var newselect:Bool!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.inx1=111
        
        inx2=0
        arrpricee.removeAllObjects()
        
        for var i in 0..<100
        {
            inx2=inx2+1000
            
            strvalue1 = String(inx2)
            arrpricee.add("\("$")\(strvalue1)")
        }
        if appDelegate.selectuser==true
        {
            arr_category_img = ["ic_search","showing1","ic_msg","ic_menu"]
            arr_category_name = ["Property Search","Showing","Messaging","Menu"]
            
            arr_category_img1 = ["ic_search","showing1","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
            arr_category_name1 = ["Property Search","Showing","Messaging","Menu","Mortgage Calculator","Contacts","Map","Suggested Properties","Settings","Share App"]
         }
        selected=false
        selectedlist=false
        selectedfilter=false
        selectedproperty=false
        selectsuggest=false
        cellleft=false
        newselect=false
        loadInitialUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
    }

    func loadInitialUI()
    {
        self.view.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        img_Backgrund=UIImageView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        img_Backgrund.image=UIImage(named: "im_bg")
        img_Backgrund.clipsToBounds=true
        img_Backgrund.alpha=0.5
        self.view.addSubview(img_Backgrund)
        
        viewtop=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: 45))
        viewtop.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.view.addSubview(viewtop)
        
        btnSearch=UIButton(frame: CGRect(x:40, y: 15, width: appDelegate.screenWidth-80, height: 25))
        btnSearch.setTitle("Search", for: .normal)
        btnSearch.setTitleColor(UIColor.gray, for: .normal)
        btnSearch.titleLabel?.font=UIFont.boldSystemFont(ofSize: 14)
        viewtop.addSubview(btnSearch)
        
        let btnmaplist=UIButton(frame: CGRect(x:appDelegate.screenWidth-130, y: 15, width: 60, height: 25))
        btnmaplist.setTitle("Map/List", for: .normal)
        btnmaplist.setTitleColor(UIColor.gray, for: .normal)
        btnmaplist.titleLabel?.font=UIFont.systemFont(ofSize: 12)
        viewtop.addSubview(btnmaplist)

        btn_filter=UIButton(frame: CGRect(x:5, y:20, width: 21, height: 20))
        btn_filter.setBackgroundImage(UIImage(named:"tool_filter"), for: .normal)
        btn_filter.tag=1
        btn_filter.addTarget(self, action: #selector(self.btn_filter_action), for: .touchUpInside)
        viewtop.addSubview(btn_filter)
        
        btn_filter1=UIButton(frame: CGRect(x:0, y:0, width: 45, height:45))
        btn_filter1.tag=1
        btn_filter1.addTarget(self, action: #selector(self.btn_filter_action), for: .touchUpInside)
        viewtop.addSubview(btn_filter1)

        let btn_fav=UIButton(frame: CGRect(x:appDelegate.screenWidth-57, y: 17, width: 20, height: 20))
        if appDelegate.selectuser==true
        {
           btn_fav.setBackgroundImage(UIImage(named:"ic_tool_home"), for: .normal)
        }
        else
        {
            btn_fav.setBackgroundImage(UIImage(named:"tool_fav"), for: .normal)
        }
        viewtop.addSubview(btn_fav)

        let btn_ref=UIButton(frame: CGRect(x:btn_fav.frame.maxX+8, y: 15, width: 25, height: 25))
        btn_ref.setBackgroundImage(UIImage(named:"tool_refresh"), for: .normal)
        btn_ref.addTarget(self, action: #selector(self.btn_ref_action), for: .touchUpInside)
        viewtop.addSubview(btn_ref)
        
        let btn_ref1=UIButton(frame: CGRect(x:btn_fav.frame.maxX, y: 7, width: 40, height: 40))
        btn_ref1.addTarget(self, action: #selector(self.btn_ref_action), for: .touchUpInside)
        viewtop.addSubview(btn_ref1)

        loadTableview()
        loadBottomCollectionview()
    }
    
    func loadTableview()
    {
        tbl_listview = UITableView()
        tbl_listview.frame = CGRect(x: 0,y:viewtop.frame.maxY,width: appDelegate.screenWidth,height:appDelegate.screenHeight-40)
      //  tbl_listview.backgroundColor=UIColor.white
        tbl_listview.delegate = self
        tbl_listview.dataSource = self
        
        tbl_listview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tbl_listview)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_listview.tableFooterView = footerView
        tbl_listview.tableFooterView?.isHidden = true
        tbl_listview.backgroundColor = UIColor.white.withAlphaComponent(0.6)

    }
    
    func loadTableviewlist()
    {
        
        imgremove=UIImageView(frame: CGRect(x: 15, y: viewtop.frame.maxY+5, width: 20, height: 20))
        imgremove.image=UIImage(named: "ic_cloase")
        self.view.addSubview(imgremove)
        
        imgshowing=UIImageView(frame: CGRect(x: appDelegate.screenWidth-35, y: viewtop.frame.maxY+5, width: 20, height: 20))
        
        imgshowing.image=UIImage(named: "img_faveret")
        self.view.addSubview(imgshowing)
        
        imgarrleft=UIImageView(frame: CGRect(x: imgremove.frame.maxX+5, y: viewtop.frame.maxY+10, width: 50, height: 10))
        imgarrleft.image=UIImage(named: "leftarrow")
        self.view.addSubview(imgarrleft)
        
        lbl_category=UILabel(frame: CGRect(x: appDelegate.screenWidth/2-100, y:viewtop.frame.maxY+2, width: 200, height: 25))
        if appDelegate.selectuser==true
        {
            imgshowing.image=UIImage(named: "ic_tool_home")
            lbl_category.text="Suggest or discard from Search"
        }
        else
        {
        lbl_category.text="Favourite or discard from Search"
        }
        lbl_category.textColor=UIColor.gray
        lbl_category.font=UIFont.systemFont(ofSize: 11)
        lbl_category.textAlignment = .center
        self.view.addSubview(lbl_category)
        
        imgarrright=UIImageView(frame: CGRect(x: appDelegate.screenWidth-90, y: viewtop.frame.maxY+10, width: 50, height: 10))
        imgarrright.image=UIImage(named: "rightarrow")
        self.view.addSubview(imgarrright)

        if appDelegate.screenWidth==320
        {
            imgarrleft.frame=CGRect(x: imgremove.frame.maxX+5, y: viewtop.frame.maxY+10, width: 40, height: 10)
            imgarrright.frame=CGRect(x: appDelegate.screenWidth-80, y: viewtop.frame.maxY+10, width: 40, height: 10)
            lbl_category.font=UIFont.systemFont(ofSize: 10)
            
        }
        tbl_listview1 = UITableView()
        tbl_listview1.frame = CGRect(x: 0,y:lbl_category.frame.maxY+5,width: appDelegate.screenWidth,height:appDelegate.screenHeight-70)
      //  tbl_listview1.backgroundColor=UIColor.white
        tbl_listview1.delegate = self
        tbl_listview1.dataSource = self
        
        tbl_listview1.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tbl_listview1)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_listview1.tableFooterView = footerView
        tbl_listview1.tableFooterView?.isHidden = true
        tbl_listview1.backgroundColor = UIColor.clear
        let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
        tbl_listview1.addGestureRecognizer(tapGestureRecognizer1)

    }
    
    func loadslideviewUI()
    {
        viewbackground1=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground1.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped5(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground1.addGestureRecognizer(tapGestureRecognizer)
        viewbackground1.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground1)

        viewslide=UIView(frame: CGRect(x: 0, y:20, width: appDelegate.screenWidth/2+60, height:appDelegate.screenHeight-20))
        viewslide.backgroundColor=UIColor.white.withAlphaComponent(0.9)
        self.view.addSubview(viewslide)
        
        let btn_filter=UIButton(frame: CGRect(x:5, y:5, width: 21, height: 20))
        btn_filter.setBackgroundImage(UIImage(named:"tool_filter"), for: .normal)
        btn_filter.tag=2
        btn_filter.addTarget(self, action: #selector(self.btn_filter_action), for: .touchUpInside)
        viewslide.addSubview(btn_filter)
        
        let btn_filter2=UIButton(frame: CGRect(x:0, y:0, width: 40, height: 35))
        btn_filter2.tag=2
        btn_filter2.addTarget(self, action: #selector(self.btn_filter_action), for: .touchUpInside)
        viewslide.addSubview(btn_filter2)
        
        let btn_applay=UIButton(frame: CGRect(x:viewslide.frame.size.width-70, y:3, width: 70, height: 20))
        btn_applay.setTitle("Apply", for: .normal)
        btn_applay.setTitleColor(UIColor.init(colorLiteralRed: 38.0/255.0, green: 95.0/255.0, blue: 128.0/255.0, alpha: 1.0), for: .normal)
        btn_applay.titleLabel?.font=UIFont.systemFont(ofSize: 15)
        btn_applay.addTarget(self, action: #selector(self.btn_filter_action), for: .touchUpInside)
        viewslide.addSubview(btn_applay)
        
        tbl_filter = UITableView()
        tbl_filter.frame = CGRect(x: 0,y:btn_filter.frame.maxY+10,width: viewslide.frame.size.width,height:viewslide.frame.size.height-30)
        //  tbl_listview1.backgroundColor=UIColor.white
        tbl_filter.delegate = self
        tbl_filter.dataSource = self
        
        tbl_filter.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        viewslide.addSubview(tbl_filter)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_filter.tableFooterView = footerView
        tbl_filter.tableFooterView?.isHidden = true
        tbl_filter.backgroundColor = UIColor.clear

    }
    
    func loadPopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewbackground)
        
        if inx==0
        {
        viewProperty=UIView(frame: CGRect(x: 15, y:80, width: appDelegate.screenWidth-30, height: 353))
        }
        else if inx==2
        {
          viewProperty=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-138, width: appDelegate.screenWidth-30, height: 277))
        }
        else if inx==3
        {
           viewProperty=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-138, width: appDelegate.screenWidth-30, height: 277))
        }
        else if inx==6
        {
            viewProperty=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-121, width: appDelegate.screenWidth-30, height: 242))
        }
        else if inx==1
        {
            viewProperty=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-103, width: appDelegate.screenWidth-30, height: 207))
        }
        else if inx==5
        {
            viewProperty=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-103, width: appDelegate.screenWidth-30, height: 207))
        }

        
        viewProperty.backgroundColor=UIColor.white
        viewProperty.layer.cornerRadius=14
        self.view.addSubview(viewProperty)

        let lbl_protype=UILabel(frame: CGRect(x:0, y:3, width:viewProperty.frame.size.width, height: 25))
        if inx==0
        {
            lbl_protype.text="Property Type"
        }
        else if inx==2
        {
            lbl_protype.text="Beds"
        }
        else if inx==3
        {
            lbl_protype.text="Baths"
        }
        else if inx==6
        {
            lbl_protype.text="Status"
        }
        else if inx==1
        {
            lbl_protype.text="Price"
            
            let lbl_from=UILabel(frame: CGRect(x:0, y:lbl_protype.frame.maxY+5, width:viewProperty.frame.size.width/2, height: 25))
            lbl_from.text="From"
            lbl_from.textColor=UIColor.black
            lbl_from.font=UIFont.systemFont(ofSize: 16)
            lbl_from.textAlignment = .center
            viewProperty.addSubview(lbl_from)
            
            let lbl_to=UILabel(frame: CGRect(x:lbl_from.frame.maxX, y:lbl_protype.frame.maxY+5, width:viewProperty.frame.size.width/2, height: 25))
            lbl_to.text="To"
            lbl_to.textColor=UIColor.black
            lbl_to.font=UIFont.systemFont(ofSize: 16)
            lbl_to.textAlignment = .center
            viewProperty.addSubview(lbl_to)
        }
        else if inx==5
        {
            lbl_protype.text="Condo Fees"
            
            let lbl_from=UILabel(frame: CGRect(x:0, y:lbl_protype.frame.maxY+5, width:viewProperty.frame.size.width/2, height: 25))
            lbl_from.text="From"
            lbl_from.textColor=UIColor.black
            lbl_from.font=UIFont.systemFont(ofSize: 16)
            lbl_from.textAlignment = .center
            viewProperty.addSubview(lbl_from)
            
            let lbl_to=UILabel(frame: CGRect(x:lbl_from.frame.maxX, y:lbl_protype.frame.maxY+5, width:viewProperty.frame.size.width/2, height: 25))
            lbl_to.text="To"
            lbl_to.textColor=UIColor.black
            lbl_to.font=UIFont.systemFont(ofSize: 16)
            lbl_to.textAlignment = .center
            viewProperty.addSubview(lbl_to)
        }
        
        lbl_protype.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbl_protype.font=UIFont.boldSystemFont(ofSize: 16)
        lbl_protype.textAlignment = .center
        viewProperty.addSubview(lbl_protype)
        
        if inx==1
        {
            tbl_property = UITableView()
            tbl_property.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+5,width: viewProperty.frame.size.width,height:140)
            
            pickerfrom=UIPickerView()
            self.pickerfrom.dataSource = self
            self.pickerfrom.delegate = self
            self.pickerfrom.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+30,width: viewProperty.frame.size.width/2,height:113)
            viewProperty.addSubview(pickerfrom)
            
            let viewneww1=UIView(frame:CGRect(x: 10, y: pickerfrom.frame.size.height/2-12.5, width: pickerfrom.frame.size.width-20, height: 25))
            viewneww1.layer.borderColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0).cgColor
            viewneww1.layer.borderWidth=1.0
            viewneww1.layer.cornerRadius=4
            pickerfrom.addSubview(viewneww1)
            
            pickerto=UIPickerView()
            self.pickerto.dataSource = self
            self.pickerto.delegate = self
            self.pickerto.frame = CGRect(x:pickerfrom.frame.maxX,y:lbl_protype.frame.maxY+30,width: viewProperty.frame.size.width/2,height:113)
            viewProperty.addSubview(pickerto)
            
            let viewneww=UIView(frame:CGRect(x: 10, y: pickerto.frame.size.height/2-12.5, width: pickerto.frame.size.width-20, height: 25))
            viewneww.layer.borderColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0).cgColor
            viewneww.layer.borderWidth=1.0
            viewneww.layer.cornerRadius=4
            pickerto.addSubview(viewneww)
        }
       else if inx==5
        {
            tbl_property = UITableView()
            tbl_property.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+5,width: viewProperty.frame.size.width,height:140)
            pickerfrom=UIPickerView()
            self.pickerfrom.dataSource = self
            self.pickerfrom.delegate = self
            self.pickerfrom.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+30,width: viewProperty.frame.size.width/2,height:113)
             pickerfrom.selectRow(3, inComponent: 0, animated: true)
            viewProperty.addSubview(pickerfrom)
            
            let viewneww1=UIView(frame:CGRect(x: 10, y: pickerfrom.frame.size.height/2-12.5, width: pickerfrom.frame.size.width-20, height: 25))
            viewneww1.layer.borderColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0).cgColor
            viewneww1.layer.borderWidth=1.0
            viewneww1.layer.cornerRadius=4
            pickerfrom.addSubview(viewneww1)

            pickerto=UIPickerView()
            self.pickerto.dataSource = self
            self.pickerto.delegate = self
            self.pickerto.frame = CGRect(x:pickerfrom.frame.maxX,y:lbl_protype.frame.maxY+30,width: viewProperty.frame.size.width/2,height:113)
            viewProperty.addSubview(pickerto)
            
            let viewneww=UIView(frame:CGRect(x: 10, y: pickerto.frame.size.height/2-12.5, width: pickerto.frame.size.width-20, height: 25))
            viewneww.layer.borderColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0).cgColor
            viewneww.layer.borderWidth=1.0
            viewneww.layer.cornerRadius=4
            pickerto.addSubview(viewneww)
        }
        else
        {
            tbl_property = UITableView()
            if inx==0
            {
                tbl_property.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+5,width: viewProperty.frame.size.width,height:280)
            }
            else if inx==2
            {
                tbl_property.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+5,width: viewProperty.frame.size.width,height:210)
            }
            else if inx==3
            {
                tbl_property.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+5,width: viewProperty.frame.size.width,height:210)
            }
            else if inx==6
            {
                tbl_property.frame = CGRect(x: 0,y:lbl_protype.frame.maxY+5,width: viewProperty.frame.size.width,height:175)
            }
        
        //  tbl_listview1.backgroundColor=UIColor.white
            tbl_property.delegate = self
            tbl_property.dataSource = self
        
            tbl_property.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            viewProperty.addSubview(tbl_property)
        
            let footerView =  UIView(frame: CGRect.zero)
            tbl_property.tableFooterView = footerView
            tbl_property.tableFooterView?.isHidden = true
            tbl_property.backgroundColor = UIColor.clear
        }
        
        let viewnew=UIView(frame: CGRect(x: 0, y: tbl_property.frame.maxY, width: viewProperty.frame.size.width, height: 1))
        viewnew.backgroundColor=UIColor.gray
        viewProperty.addSubview(viewnew)
        
        let btncancel=UIButton(frame: CGRect(x: 0, y:viewnew.frame.maxY, width: viewProperty.frame.size.width/2-1, height: 39))
        btncancel.setTitle("Cancel", for: .normal)
        btncancel.setTitleColor(UIColor.gray, for: .normal)
        btncancel.titleLabel?.font=UIFont.systemFont(ofSize: 16)
        btncancel.addTarget(self, action: #selector(self.btncancel_action), for: .touchUpInside)
        viewProperty.addSubview(btncancel)
        
        viewnew1=UIView(frame: CGRect(x:btncancel.frame.maxX, y: viewnew.frame.maxY, width: 1, height: 39))
        viewnew1.backgroundColor=UIColor.gray
        viewProperty.addSubview(viewnew1)

        let btnapply=UIButton(frame: CGRect(x:viewnew1.frame.maxX, y: viewnew.frame.maxY, width: viewProperty.frame.size.width/2, height: 39))
        btnapply.setTitle("Apply", for: .normal)
        btnapply.setTitleColor(UIColor.gray, for: .normal)
        btnapply.titleLabel?.font=UIFont.systemFont(ofSize: 16)
        btnapply.addTarget(self, action: #selector(self.btncancel_action), for: .touchUpInside)
        viewProperty.addSubview(btnapply)

    }
    
    func loadsuggestPopupUI()
    {
        selectsuggest=true
        selectedlist=false
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewSuggest=UIView(frame: CGRect(x: 15, y:25, width: appDelegate.screenWidth-30, height: appDelegate.screenHeight-80))
        viewSuggest.backgroundColor=UIColor.white
        viewSuggest.layer.cornerRadius=14
        self.view.addSubview(viewSuggest)

        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewSuggest.frame.size.width, height: 30))
        lbltitle.text="Suggest Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewSuggest.addSubview(lbltitle)
 
        let viewback2=UIView(frame: CGRect(x: 16, y: lbltitle.frame.maxY+6, width: viewSuggest.frame.size.width-32, height: 28))
        viewback2.backgroundColor=UIColor.white
        viewSuggest.addSubview(viewback2)
        
        viewback2.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: viewback2.bounds, cornerRadius: 8)
        
        viewback2.layer.masksToBounds = false
        viewback2.layer.shadowColor = shadowColor?.cgColor
        viewback2.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewback2.layer.shadowOpacity = shadowOpacity
        viewback2.layer.shadowPath = shadowPath.cgPath

        txtSearch=UITextField(frame: CGRect(x: 15, y: lbltitle.frame.maxY+5, width: viewSuggest.frame.size.width-30, height: 30))
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search", attributes:[NSForegroundColorAttributeName: UIColor.black.withAlphaComponent(0.6),NSFontAttributeName :UIFont(name: "Arial", size: 16)!])
        txtSearch.textColor=UIColor.black.withAlphaComponent(0.8)
        txtSearch.font=UIFont.systemFont(ofSize: 16)
        txtSearch.textAlignment = .center
        txtSearch.layer.cornerRadius=8
        txtSearch.backgroundColor=UIColor.white
        viewSuggest.addSubview(txtSearch)
        
        let btnsarch=UIButton(frame: CGRect(x: 3, y: 5, width: 20, height: 20))
        btnsarch.setBackgroundImage(UIImage(named:"ic_agent_search"), for: .normal)
        txtSearch.addSubview(btnsarch)
        
        tbl_suggest = UITableView()
        tbl_suggest.frame = CGRect(x: 0,y:txtSearch.frame.maxY+5,width: viewSuggest.frame.size.width,height:viewSuggest.frame.size.height-105)
        //  tbl_listview1.backgroundColor=UIColor.white
        tbl_suggest.delegate = self
        tbl_suggest.dataSource = self
        
        tbl_suggest.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        viewSuggest.addSubview(tbl_suggest)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_suggest.tableFooterView = footerView
        tbl_suggest.tableFooterView?.isHidden = true
        tbl_suggest.backgroundColor = UIColor.clear

        let viewnew2=UIView(frame: CGRect(x:0, y: viewSuggest.frame.size.height-38.7, width:viewSuggest.frame.size.width, height: 0.7))
        viewnew2.backgroundColor=UIColor.gray
        viewSuggest.addSubview(viewnew2)

        let btncancel=UIButton(frame: CGRect(x: 0, y:viewSuggest.frame.size.height-39, width: viewSuggest.frame.size.width/2-1, height: 39))
        btncancel.setTitle("Cancel", for: .normal)
        btncancel.setTitleColor(UIColor.gray, for: .normal)
        btncancel.titleLabel?.font=UIFont.systemFont(ofSize: 16)
        btncancel.addTarget(self, action: #selector(self.btncancel_action1), for: .touchUpInside)
        viewSuggest.addSubview(btncancel)
        
        let viewnew1=UIView(frame: CGRect(x:btncancel.frame.maxX, y: viewSuggest.frame.size.height-38, width: 0.7, height: 39))
        viewnew1.backgroundColor=UIColor.gray
        viewSuggest.addSubview(viewnew1)
        
        let btnapply=UIButton(frame: CGRect(x:viewnew1.frame.maxX, y: viewSuggest.frame.size.height-39, width: viewSuggest.frame.size.width/2, height: 39))
        btnapply.setTitle("Suggest", for: .normal)
        btnapply.setTitleColor(UIColor.gray, for: .normal)
        btnapply.titleLabel?.font=UIFont.systemFont(ofSize: 16)
        btnapply.addTarget(self, action: #selector(self.btncancel_action1), for: .touchUpInside)
        viewSuggest.addSubview(btnapply)
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-80, width: appDelegate.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)
        
        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)
        
        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)
        
        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
    }
    
    func loadRemovePopup()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-70, width: appDelegate.screenWidth-30, height: 140))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Remove Listing?"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let lbltitle1=UILabel(frame: CGRect(x: 10, y:lbltitle.frame.maxY, width: viewshare.frame.size.width-20, height: 30))
        lbltitle1.text="This will remove the listing from your favourites"
        lbltitle1.textColor=UIColor.black.withAlphaComponent(0.7)
        lbltitle1.font=UIFont.systemFont(ofSize: 13)
        lbltitle1.textAlignment = .center
        lbltitle1.numberOfLines=3
        viewshare.addSubview(lbltitle1)
        
        let lbltitle2=UILabel(frame: CGRect(x: 10, y:lbltitle1.frame.maxY+5, width: viewshare.frame.size.width-20, height: 20))
        lbltitle2.text="Don't show this message again"
        lbltitle2.textColor=UIColor.black.withAlphaComponent(0.7)
        lbltitle2.font=UIFont.systemFont(ofSize: 13)
        lbltitle2.textAlignment = .center
        lbltitle2.numberOfLines=3
        viewshare.addSubview(lbltitle2)
        
        let imgdot=UIImageView(frame: CGRect(x:viewshare.frame.size.width-40, y:lbltitle1.frame.maxY+5, width: 20, height: 20))
        imgdot.image=UIImage(named:"ic_dot1")
        viewshare.addSubview(imgdot)
        
        let middleview = UIView(frame: CGRect(x:0, y:viewshare.frame.size.height-36 ,width:viewshare.frame.size.width, height:0.6))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewshare.addSubview(middleview)
        
        
        let btncancle = UIButton(frame:CGRect(x:5, y:middleview.frame.maxY, width:viewshare.frame.size.width/2-20, height:30))
        btncancle.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btncancle.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btncancle.addTarget(self, action:#selector(btncancelclick), for: UIControlEvents.touchUpInside)
        btncancle.setTitle("Cancle", for: UIControlState.normal)
        viewshare.addSubview(btncancle)
        
        
        let btnApply = UIButton(frame:CGRect(x:btncancle.frame.maxX+25 , y:middleview.frame.maxY, width:viewshare.frame.size.width/2-10, height:30))
        btnApply.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnApply.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnApply.addTarget(self, action:#selector(btncancelclick), for: UIControlEvents.touchUpInside)
        btnApply.setTitle("Remove", for: UIControlState.normal)
        viewshare.addSubview(btnApply)
        
        let lineview = UIView(frame: CGRect(x:btncancle.frame.maxX+8, y:viewshare.frame.size.height-36,width:0.6, height:36))
        lineview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        viewshare.addSubview(lineview)
        
    }


    
    func loadBottomCollectionview()
    {
        viewexpand=UIView(frame: CGRect(x: 0, y: appDelegate.screenHeight-80, width: appDelegate.screenWidth, height: 80))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if appDelegate.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:80), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if appDelegate.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
            img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
            img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
            img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if appDelegate.screenWidth==320
        {
             lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        
        if selected==true
        {
            lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
            lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-80, width: appDelegate.screenWidth, height: 80)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:80), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-210, width: appDelegate.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==6
        {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row==1
        {
            if appDelegate.selectuser==true
            {
                let showingView = ShowingView(nibName: "ShowingView", bundle: nil)
                navigationController?.pushViewController(showingView, animated: true)
            }
            else
            {
                let fevriteView = FavriteView(nibName: "FavriteView", bundle: nil)
                navigationController?.pushViewController(fevriteView, animated: true)
            }
        }
        else if indexPath.row==5
        {
            if appDelegate.selectuser==true
            {
                let contactView = ContactsView(nibName: "ContactsView", bundle: nil)
                navigationController?.pushViewController(contactView, animated: true)
            }
            else
            {
                let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
                navigationController?.pushViewController(agentView, animated: true)
            }
        }
        else if indexPath.row==7
        {
            let suggestedView = SuggestedProperties(nibName: "SuggestedProperties", bundle: nil)
            navigationController?.pushViewController(suggestedView, animated: true)
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==8
        {
            let settingView = SettingsView(nibName: "SettingsView", bundle: nil)
            navigationController?.pushViewController(settingView, animated: true)
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }

    }
    
    //UITableview Delegate and DataSource methods
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if selectedlist==true
        {
            return 10
        }
        else if selectedfilter==true
        {
            return arr_filter_name.count
        }
        else if selectedproperty==true
        {
            return arr_property_type.count
        }
        else if selectsuggest==true
        {
            return 10
        }
        else
        {
         return 12
        }
       // return 0
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyTestCell")
        
        if selectedlist==true
        {
            cell.backgroundColor=UIColor.clear
            tbl_listview1.separatorColor=UIColor.clear
            
            if inx1==indexPath.row
            {
             viewback1=UIView(frame: CGRect(x: 130, y: 10, width: appDelegate.screenWidth-220, height: appDelegate.screenWidth-66))
            }
            else
            {
            viewback1=UIView(frame: CGRect(x: 16, y: 10, width: appDelegate.screenWidth-32, height: appDelegate.screenWidth-66))
            }
            viewback1.backgroundColor=UIColor.white
            viewback1.layer.cornerRadius=16
            cell.addSubview(viewback1)
            
            viewback1.layer.cornerRadius = cornerRadius
            let shadowPath = UIBezierPath(roundedRect: viewback1.bounds, cornerRadius: cornerRadius)
            
            viewback1.layer.masksToBounds = false
            viewback1.layer.shadowColor = shadowColor?.cgColor
            viewback1.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
            viewback1.layer.shadowOpacity = shadowOpacity
            viewback1.layer.shadowPath = shadowPath.cgPath

            if inx1==indexPath.row
            {
                viewback=UIView(frame: CGRect(x: 129, y: 5, width: appDelegate.screenWidth-30, height: appDelegate.screenWidth-60))
                let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped3(tapGestureRecognizer:)))
                tapGestureRecognizer1.direction = UISwipeGestureRecognizerDirection.left
                viewback.addGestureRecognizer(tapGestureRecognizer1)
            }
            else
            {
            viewback=UIView(frame: CGRect(x: 15, y: 5, width: appDelegate.screenWidth-30, height: appDelegate.screenWidth-60))
            }
            viewback.backgroundColor=UIColor.white
            viewback.layer.cornerRadius=16
            cell.addSubview(viewback)
            
            let lblprice=UILabel(frame: CGRect(x: viewback.frame.size.width-120, y: 0, width: 100, height: 30))
            lblprice.text="$512,111"
            lblprice.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            lblprice.font=UIFont.boldSystemFont(ofSize: 20)
            lblprice.textAlignment = .right
            viewback.addSubview(lblprice)
            
            let lblkm=UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: 30))
            lblkm.text="1.2 km"
            lblkm.textColor=UIColor.black.withAlphaComponent(0.7)
            lblkm.font=UIFont.systemFont(ofSize: 13)
            lblkm.textAlignment = .left
            viewback.addSubview(lblkm)
 
            let img_home=UIImageView(frame: CGRect(x:5, y: 30, width: viewback.frame.size.width-10, height: appDelegate.screenWidth-140))
            img_home.image=UIImage(named: "home")
            img_home.clipsToBounds=true
            viewback.addSubview(img_home)
            
            let lbladd=UILabel(frame: CGRect(x: 10, y:img_home.frame.maxY, width: viewback.frame.size.width-20, height: 25))
            lbladd.text="123 Fariview Park Trial, Calgary AB, T6F G9Y"
            lbladd.textColor=UIColor.black.withAlphaComponent(0.7)
            lbladd.font=UIFont.systemFont(ofSize: 13)
            lbladd.textAlignment = .left
            viewback.addSubview(lbladd)
            let btn_map=UIButton(frame: CGRect(x: 10, y: lbladd.frame.maxY+2, width: 14, height: 15))
            btn_map.setBackgroundImage(UIImage(named:"img_map_pointer"), for: .normal)
            btn_map.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
            viewback.addSubview(btn_map)
            
            let btn_mapview=UIButton(frame: CGRect(x:btn_map.frame.maxX, y: lbladd.frame.maxY, width: 60, height: 20))
            btn_mapview.setTitle("Map View", for: .normal)
            btn_mapview.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
            btn_mapview.titleLabel?.font=UIFont.systemFont(ofSize: 11)
            btn_mapview.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
            viewback.addSubview(btn_mapview)
            
            let viewnew=UIView(frame: CGRect(x: UIScreen.main.bounds.size.width/100*104, y: viewback.frame.size.height/2-50, width: 90, height: 100))
            viewnew.backgroundColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            viewnew.layer.cornerRadius=7
            cell.addSubview(viewnew)
            
            let btn_remove_img=UIButton(frame: CGRect(x: 25, y: 20, width: 25, height: 25))
            btn_remove_img.setBackgroundImage(UIImage(named:"ic_discard"), for: .normal)
            btn_remove_img.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
            viewnew.addSubview(btn_remove_img)

            let btn_remove=UIButton(frame: CGRect(x:0, y: 60, width:75, height: 30))
            btn_remove.setTitle("Discard", for: .normal)
            btn_remove.setTitleColor(UIColor.white, for: .normal)
            btn_remove.titleLabel?.font=UIFont.systemFont(ofSize: 15)
            btn_remove.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
            viewnew.addSubview(btn_remove)
     
            print(inx1)
            if indexPath.row==inx1
            {
                viewnew1=UIView(frame: CGRect(x:0, y:viewback.frame.size.height/2-50, width:110, height: 100))
            }
            else
           {
                viewnew1=UIView(frame: CGRect(x:-132, y:viewback.frame.size.height/2-50, width:110, height: 100))
            }
            viewnew1.backgroundColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            viewnew1.layer.cornerRadius=7
            cell.addSubview(viewnew1)
            
            let btn_remove_img1=UIButton(frame: CGRect(x: viewnew1.frame.size.width/2-16, y: 20, width: 40, height: 40))
            
            btn_remove_img1.setBackgroundImage(UIImage(named:"ic_swipe_favorite1"), for: .normal)
            // btn_remove_img.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
            viewnew1.addSubview(btn_remove_img1)
            
            let btn_remove1=UIButton(frame: CGRect(x:0, y: 60, width:110, height: 30))
            btn_remove1.setTitle("Favourite", for: .normal)
            btn_remove1.setTitleColor(UIColor.white, for: .normal)
            btn_remove1.titleLabel?.font=UIFont.systemFont(ofSize: 13)
            // btn_remove.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
            viewnew1.addSubview(btn_remove1)

            if appDelegate.selectuser==true
            {
               // cell.isUserInteractionEnabled=true
               // viewnew1.isUserInteractionEnabled=true
               // btn_remove_img1.isUserInteractionEnabled = true
                //btn_remove1.isUserInteractionEnabled = true
                
                btn_remove.setTitle("Remove", for: .normal)
                
                btn_remove_img1.setBackgroundImage(UIImage(named:"ic_swipe_home"), for: .normal)
                btn_remove1.setTitle("Suggest", for: .normal)
                btn_remove_img1.addTarget(self, action: #selector(self.btn_suggest_action), for: .touchUpInside)
                btn_remove1.addTarget(self, action: #selector(self.btn_suggest_action), for: .touchUpInside)
            }

//cell.accessoryView
        }
        else if selectedfilter==true
        {
            tbl_filter.separatorColor=UIColor.clear
            cell.backgroundColor=UIColor.clear
            
            let lblfiltr=UILabel(frame: CGRect(x:5, y:5, width: 200, height: 30))
            lblfiltr.text=arr_filter_name[indexPath.row]
            lblfiltr.textColor=UIColor.black.withAlphaComponent(0.5)
            lblfiltr.font=UIFont.boldSystemFont(ofSize: 14)
            lblfiltr.textAlignment = .left
            cell.addSubview(lblfiltr)
            if indexPath.row==4
            {
                let imgdot=UIImageView(frame: CGRect(x:tbl_filter.frame.size.width-40, y:7.5, width: 20, height: 20))
                imgdot.image=UIImage(named:"ic_dot1")
                cell.addSubview(imgdot)
            }
            else
            {
            let lblany=UILabel(frame: CGRect(x:tbl_filter.frame.size.width-100, y:5, width: 80, height: 30))
            lblany.text="Any"
            lblany.textColor=UIColor.black.withAlphaComponent(0.7)
            lblany.font=UIFont.systemFont(ofSize: 12)
            lblany.textAlignment = .right
            cell.addSubview(lblany)
            }
        }
        else if selectedproperty==true
        {
           tbl_property.separatorColor=UIColor.clear
            cell.backgroundColor=UIColor.clear
            
            let lblfiltr=UILabel(frame: CGRect(x:10, y:2.5, width: 200, height: 30))
            lblfiltr.text=arr_property_type[indexPath.row]
            lblfiltr.textColor=UIColor.black.withAlphaComponent(0.5)
            lblfiltr.font=UIFont.boldSystemFont(ofSize: 14)
            lblfiltr.textAlignment = .left
            cell.addSubview(lblfiltr)
            
            let imgdot=UIButton(frame: CGRect(x:tbl_property.frame.size.width-40, y:7.5, width: 20, height: 20))
            if indexPath.row==0
            {
                imgdot.setBackgroundImage(UIImage(named:"ic_dot"), for: .normal)
               
            }
            else
            {
                imgdot.setBackgroundImage(UIImage(named:"ic_dot1"), for: .normal)
               
            }
            imgdot.tag=indexPath.row
            imgdot.addTarget(self, action: #selector(self.imgdot_action), for: .touchUpInside)
            cell.addSubview(imgdot)
        }
        else if selectsuggest==true
        {
            let img_home=UIImageView(frame: CGRect(x:10, y: 5, width: 50, height: 50))
            img_home.image=UIImage(named: "agent_pic")
            img_home.layer.cornerRadius=8
            img_home.clipsToBounds=true
            cell.addSubview(img_home)
            
            let lblname=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y:3, width: 200, height: 20))
            lblname.text="Ashley Jenkins"
            lblname.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            lblname.font=UIFont.systemFont(ofSize: 14)
            lblname.textAlignment = .left
            cell.addSubview(lblname)

            let lbladd=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y:lblname.frame.maxY-3, width: 120, height: 20))
            lbladd.text="1-44-444442154"
            lbladd.textColor=UIColor.black.withAlphaComponent(0.7)
            lbladd.font=UIFont.systemFont(ofSize: 11)
            lbladd.textAlignment = .left
            cell.addSubview(lbladd)
            
            let lbladds=UILabel(frame: CGRect(x: lbladd.frame.maxX+5, y:lblname.frame.maxY-3, width: 100, height: 20))
            lbladds.text="babs@gmail.com"
            lbladds.textColor=UIColor.black.withAlphaComponent(0.7)
            lbladds.font=UIFont.systemFont(ofSize: 11)
            //lbladds.numberOfLines=3
            lbladds.textAlignment = .left
            cell.addSubview(lbladds)

            if arrselected.contains(indexPath.row)
            {
                cell.backgroundColor=UIColor.gray.withAlphaComponent(0.4)
            }
        }
        else
        {
            cell.backgroundColor=UIColor.clear
        let img_home=UIImageView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth/2-50, height: 86))
        img_home.image=UIImage(named: "home")
        img_home.clipsToBounds=true
        cell.addSubview(img_home)

        let btn_heart=UIButton(frame: CGRect(x: img_home.frame.maxX+5, y: 4, width: 18, height: 18))
        btn_heart.setBackgroundImage(UIImage(named:"img_faveret"), for: .normal)
        cell.addSubview(btn_heart)
        //img_map_pointer
        let btn_close=UIButton(frame: CGRect(x: btn_heart.frame.maxX+8, y: 3, width: 27, height: 24))
        btn_close.setBackgroundImage(UIImage(named:"ic_cloase"), for: .normal)
        cell.addSubview(btn_close)
        
        let lblprice=UILabel(frame: CGRect(x: appDelegate.screenWidth-125, y: 0, width: 100, height: 30))
        lblprice.text="$512,111"
        lblprice.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblprice.font=UIFont.boldSystemFont(ofSize: 19)
        lblprice.textAlignment = .right
        cell.addSubview(lblprice)
        
        let lbladress=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y: btn_heart.frame.maxY+2, width: 200, height: 20))
        lbladress.text="123 Fariview Park..."
        lbladress.textColor=UIColor.black.withAlphaComponent(0.6)
        lbladress.font=UIFont.systemFont(ofSize: 11)
        lbladress.textAlignment = .left
        cell.addSubview(lbladress)

        let lbladres=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y: lbladress.frame.maxY-3, width: 200, height: 20))
        lbladres.text="Balthasaur Point,AB T6Q 9W1"
        lbladres.textColor=UIColor.black.withAlphaComponent(0.6)
        lbladres.font=UIFont.systemFont(ofSize: 11)
        lbladres.textAlignment = .left
        cell.addSubview(lbladres)

        let lblflower=UILabel(frame: CGRect(x: img_home.frame.maxX+5, y: lbladres.frame.maxY+5, width: 200, height: 20))
        lblflower.text="2 fl|4 bd|3 ba|12,000 sq"
        lblflower.textColor=UIColor.black.withAlphaComponent(0.6)
        lblflower.font=UIFont.systemFont(ofSize: 11)
        lblflower.textAlignment = .left
        cell.addSubview(lblflower)

        let btn_map=UIButton(frame: CGRect(x: appDelegate.screenWidth-80, y: lbladres.frame.maxY+6, width: 14, height: 15))
        btn_map.setBackgroundImage(UIImage(named:"img_map_pointer"), for: .normal)
        btn_map.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        cell.addSubview(btn_map)

        let btn_mapview=UIButton(frame: CGRect(x:btn_map.frame.maxX, y: lbladres.frame.maxY+4, width: 60, height: 20))
        btn_mapview.setTitle("Map View", for: .normal)
        btn_mapview.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
        btn_mapview.titleLabel?.font=UIFont.systemFont(ofSize: 11)
        btn_mapview.addTarget(self, action: #selector(self.btn_map_action), for: .touchUpInside)
        cell.addSubview(btn_mapview)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedlist==true
        {
            return appDelegate.screenWidth-50
        }
        else if selectedfilter==true
        {
            return 40
        }
        else if selectedproperty==true
        {
            return 35
        }
        else if selectsuggest==true
        {
            return 60
        }
        else
        {
        return 86
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if selectsuggest==true
        {
            let cell = tbl_suggest.cellForRow(at: indexPath);
            if arrselected.contains(indexPath.row)
            {
                arrselected.remove(indexPath.row)
                cell?.backgroundColor=UIColor.clear
            }
            else
            {
                arrselected.add(indexPath.row)
                cell?.backgroundColor=UIColor.gray.withAlphaComponent(0.4)
            }
        }
        if cellleft==true
        {
            
        }
        else
        {
            if selectedlist==true
            {
                tbl_listview1.reloadData()
                let detailView = DetailViewController(nibName: "DetailViewController", bundle: nil)
                navigationController?.pushViewController(detailView, animated: true)
            }
        }
       
        
        if cellleft==true
        {
            inx1=111
            cellleft=false
            tbl_listview1.reloadData()
        }
        
        if selectedfilter==true
        {
            if indexPath.row==0
            {
                arr_property_type = ["Single-Family Home","Condo / Townhouse","Rental","Lots","Mobile Homes","Multi-Family","Commercial","Industrial"]
                inx=indexPath.row
                selectedfilter=false
                selectedproperty=true
                loadPopupUI()
            }
            else if indexPath.row==2
            {
                arr_property_type = ["1+","2+","3+","4+","5+","6+"]
                selectedfilter=false
                selectedproperty=true
                inx=indexPath.row
                loadPopupUI()
            }
            else if indexPath.row==3
            {
                arr_property_type = ["1+","2+","3+","4+","5+","6+"]
                selectedfilter=false
                selectedproperty=true
                inx=indexPath.row
                loadPopupUI()
            }
            else if indexPath.row==6
            {
                arr_property_type = ["Any","Active","Pending","Sold","Back Up Offer"]
                selectedfilter=false
                selectedproperty=true
                inx=indexPath.row
                loadPopupUI()
            }
            else if indexPath.row==1
            {
                inx=indexPath.row
                loadPopupUI()
            }
            else if indexPath.row==5
            {
                inx=indexPath.row
                loadPopupUI()
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //if editingStyle == UITableViewCellEditingStyle.delete {
       //     numbers.removeAtIndex(indexPath.row)
           // tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
       // }
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        if tableView==tbl_listview1
        {
            if cellleft==true
            {
                inx1=111
                cellleft=false
                
                tbl_listview1.reloadData()
            }
            else
            {
                let more = UITableViewRowAction(style: .normal, title: "             ") { action, index in
                    
                    self.loadRemovePopup()
                    //self.loadsuggestPopupUI()
                    //tableView.setEditing(true, animated: false)
                }
            
                more.backgroundColor=UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
                return [more]
            }
        }
        else
        {
            return nil
        }
        return nil
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView==tbl_listview1
        {
            if cellleft==true
            {
                return false
            }
            else
            {
            return true
            }
        }
        else
        {
            return false
        }
        
    }
    
    func btn_ref_action(sender:UIButton)
    {
        btn_filter.isHidden=false
        btn_filter1.isHidden=false
        btnSearch.isHidden=false
        selectedfilter=false
        viewslide.removeFromSuperview()
     if selectedlist==true
     {
        img_Backgrund.isHidden=false
        img_Backgrund.alpha=0.5
        selectedlist=false
       // tbl_listview.isHidden=false
        imgremove.removeFromSuperview()
        imgshowing.removeFromSuperview()
        imgarrleft.removeFromSuperview()
        lbl_category.removeFromSuperview()
        imgarrright.removeFromSuperview()

        tbl_listview.removeFromSuperview()
        tbl_listview1.removeFromSuperview()
        loadTableview()
        self.view.addSubview(viewexpand)
     }
     else
     {
      self.view.backgroundColor = UIColor.init(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        img_Backgrund.isHidden=true
      //  img_Backgrund.alpha=0.2
        //tbl_listview.isHidden=true
        selectedlist=true
        tbl_listview.removeFromSuperview()
        tbl_listview1.removeFromSuperview()
        imgremove.removeFromSuperview()
        imgshowing.removeFromSuperview()
        imgarrleft.removeFromSuperview()
        lbl_category.removeFromSuperview()
        imgarrright.removeFromSuperview()
        
        loadTableviewlist()
        self.view.addSubview(viewexpand)
    }
    }
    
    func btn_map_action(sender:UIButton)
    {
        let mapView = DetailMapview(nibName: "DetailMapview", bundle: nil)
        navigationController?.pushViewController(mapView, animated: true)
       // _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func btn_filter_action(sender:UIButton)
    {
        
        if sender.tag==1
        {
            newselect=false
            if selectedlist==true
            {
                newselect=true
            }
            btn_filter.isHidden=true
            btn_filter1.isHidden=true
            btnSearch.isHidden=true
            selectedlist=false
            
            selectedfilter=true
            loadslideviewUI()
        }
        else
        {
            self.viewbackground1.removeFromSuperview()
            selectedfilter=false
            btn_filter.isHidden=false
            btn_filter1.isHidden=false
            btnSearch.isHidden=false
            if newselect==true
            {
              selectedlist=true
            }
            viewslide.removeFromSuperview()
        }
    }
    func btncancel_action(sender:UIButton)
    {
        selectedproperty=false
        selectedfilter=true
        viewbackground.removeFromSuperview()
        viewProperty.removeFromSuperview()
    }
    
    func btncancel_action1(sender:UIButton)
    {
        selectsuggest=false
        selectedlist=true
        viewbackground.removeFromSuperview()
        viewSuggest.removeFromSuperview()
        tbl_suggest.removeFromSuperview()
    }
    
    func btn_suggest_action(sender:UIButton)
    {
        self.arrselected.removeAllObjects()
       loadsuggestPopupUI()
    }
    
    //Pickerview Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrpricee.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      strvalue = arrpricee[row] as! String
      //  strvalue=strvalue
        
        return strvalue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       // txt_select_service.text = arr_reminder_day[row]
    }
    
    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
           
        }
    }
    
    func imageTapped5(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground1.removeFromSuperview()
            self.selectedfilter=false
            self.btn_filter.isHidden=false
            self.btn_filter1.isHidden=false
            self.btnSearch.isHidden=false
            if self.newselect==true
            {
                self.selectedlist=true
            }
            self.viewslide.removeFromSuperview()
        }
    }
    
    func imageTapped2(tapGestureRecognizer: UISwipeGestureRecognizer) {
        self.inx1=111
        cellleft=true
        tbl_listview1.reloadData()
        if tapGestureRecognizer.state == UIGestureRecognizerState.ended {
            let swipeLocation = tapGestureRecognizer.location(in: self.tbl_listview1)
            if let swipedIndexPath = tbl_listview1.indexPathForRow(at: swipeLocation) {
                if let swipedCell = self.tbl_listview1.cellForRow(at: swipedIndexPath) {
                    //let cell = tbl_listview1.cellForRow(at: swipedIndexPath);
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        let cellIndexPath = self.tbl_listview1.indexPathForRow(at: swipeLocation)
                        self.inx1 = cellIndexPath!.row
                        print(self.inx1)
                        self.tbl_listview1.reloadData()
                       // swipedCell.frame = CGRect(x: swipedCell.frame.origin.x + 120, y: swipedCell.frame.origin.y, width: swipedCell.bounds.size.width - 120, height: swipedCell.bounds.size.height)
                     //   self.viewback1.frame=CGRect(x: 125, y: 10, width: self.appDelegate.screenWidth-32, height: self.appDelegate.screenWidth-66)
                     //   self.viewback.frame=CGRect(x: 125, y: 5, width: self.appDelegate.screenWidth-30, height: self.appDelegate.screenWidth-60)
                       // swipedCell.addSubview(self.viewback1)
                       // swipedCell.addSubview(self.viewback)
                    })
                    
                    let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped3(tapGestureRecognizer:)))
                    tapGestureRecognizer1.direction = UISwipeGestureRecognizerDirection.left
                    swipedCell.addGestureRecognizer(tapGestureRecognizer1)
                }
            }
        }
    }
    
    func loadtable()
    {
        tbl_listview1.removeFromSuperview()
        tbl_listview1 = UITableView()
        tbl_listview1.frame = CGRect(x: 0,y:lbl_category.frame.maxY+5,width: appDelegate.screenWidth,height:appDelegate.screenHeight-70)
        //  tbl_listview1.backgroundColor=UIColor.white
        tbl_listview1.delegate = self
        tbl_listview1.dataSource = self
        
        tbl_listview1.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tbl_listview1)
        
        let footerView =  UIView(frame: CGRect.zero)
        tbl_listview1.tableFooterView = footerView
        tbl_listview1.tableFooterView?.isHidden = true
        tbl_listview1.backgroundColor = UIColor.clear
        let tapGestureRecognizer1 = UISwipeGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
        tbl_listview1.addGestureRecognizer(tapGestureRecognizer1)
 
    }
    
    func imageTapped3(tapGestureRecognizer: UISwipeGestureRecognizer) {
        self.inx1=111
        //UIView.animate(withDuration: 0.3, animations: {
            self.cellleft=false
           // self.tbl_listview1.reloadData()

            self.tbl_listview1.removeFromSuperview()
            self.loadtable()
            self.view.addSubview(self.viewexpand)
            
       // }, completion: { (finished) in
       // })
        
    }
    
    func btncancelclick(sender:UIButton)
    {
        viewbackground.removeFromSuperview()
        viewshare.removeFromSuperview()
    }
    
    func imgdot_action(sender:UIButton)
    {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
