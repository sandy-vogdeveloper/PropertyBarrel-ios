//
//  ViewController.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/2/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController ,MKMapViewDelegate ,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate{
    
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?

    var locationmanager:CLLocationManager!
    
    
  
    var annotation:MKAnnotation!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!

    var appDelegate:AppDelegate!
    
    var viewexpand:UIView!
    var viewbackground:UIView!
    var viewshare:UIView!
    var viewtop:UIView!
    
    var lbl_title:UILabel!
    var lbl_category_name:UILabel!
    
    var mapview:MKMapView!
    
    var expandcollectionView:UICollectionView!
    var layout:UICollectionViewFlowLayout!
    var selected:Bool!
    
    
    
    var arr_category_img:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home"]
    var arr_category_name:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties"]
    
    var arr_category_img1:[String] = ["ic_search","ic_fev","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
    var arr_category_name1:[String] = ["Property Search","Favorites","Messaging","Menu","Mortgage Calculator","Agents","Map","Suggested Properties","Settings","Share App"]
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
         appDelegate = UIApplication.shared.delegate as! AppDelegate
        selected=false
        
        if appDelegate.selectuser==true
        {
           arr_category_img = ["ic_search","showing1","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home"]
            arr_category_name = ["Property Search","Showing","Messaging","Menu","Mortgage Calculator","Contacts","Map","Suggested Properties"]
            
          arr_category_img1 = ["ic_search","showing1","ic_msg","ic_menu","cal","ic_agent","ic_map","ic_home","ic_setting","ic_shearapp"]
           arr_category_name1 = ["Property Search","Showing","Messaging","Menu","Mortgage Calculator","Contacts","Map","Suggested Properties","Settings","Share App"]
        }
//       // mapView.delegate = self
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        
//        // Check for Location Services
//        
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.startUpdatingLocation()
//        }
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationmanager = CLLocationManager()
            locationmanager.delegate = self
            locationmanager.desiredAccuracy = kCLLocationAccuracyBest
            locationmanager.requestWhenInUseAuthorization()
            locationmanager.startUpdatingLocation()
        }
       
        loadInitialUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
    }
    
    func loadInitialUI()
    {
        mapview=MKMapView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        mapview.delegate=self
        mapview.mapType = MKMapType.standard
        mapview.isZoomEnabled = true
        mapview.isScrollEnabled = true
        mapview.showsUserLocation = true
        mapview.isPitchEnabled = true
        mapview.isRotateEnabled = true
        //mapview.center = view.center
        self.view.addSubview(mapview)

        viewtop=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: 40))
        viewtop.backgroundColor=UIColor.init(colorLiteralRed: 38.0/255.0, green: 95.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        self.view.addSubview(viewtop)
        
        lbl_title=UILabel(frame: CGRect(x: 0, y: 10, width: appDelegate.screenWidth, height: 30))
        lbl_title.text="Instaview"
        lbl_title.textColor=UIColor.white
        lbl_title.backgroundColor=UIColor.init(colorLiteralRed: 38.0/255.0, green: 95.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        lbl_title.font=UIFont.boldSystemFont(ofSize: 17)
        lbl_title.textAlignment = .center
        viewtop.addSubview(lbl_title)
        
       loadBottomCollectionview() 
    }
    
    func laodSharePopupUI()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        // gestureRecognizer.delegate = self
        viewbackground.addGestureRecognizer(tapGestureRecognizer)
        viewbackground.isUserInteractionEnabled=true
        self.view.addSubview(viewbackground)
        
        viewshare=UIView(frame: CGRect(x: 15, y:appDelegate.screenHeight/2-80, width: appDelegate.screenWidth-30, height: 160))
        viewshare.backgroundColor=UIColor.white
        viewshare.layer.cornerRadius=14
        self.view.addSubview(viewshare)
        
        let lbltitle=UILabel(frame: CGRect(x: 0, y:0, width: viewshare.frame.size.width, height: 30))
        lbltitle.text="Share Listing"
        lbltitle.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltitle.font=UIFont.systemFont(ofSize: 15)
        lbltitle.textAlignment = .center
        viewshare.addSubview(lbltitle)
        
        let btnfb=UIButton(frame: CGRect(x: 20, y: 50, width: 40, height: 40))
        btnfb.setBackgroundImage(UIImage(named:"fb"), for: .normal)
        viewshare.addSubview(btnfb)
        
        let btntext=UIButton(frame: CGRect(x: viewshare.frame.size.width/2-20, y: 50, width: 40, height: 40))
        btntext.setBackgroundImage(UIImage(named:"text"), for: .normal)
        viewshare.addSubview(btntext)

        let btnemail=UIButton(frame: CGRect(x: viewshare.frame.size.width-60, y: 55, width: 40, height: 30))
        btnemail.setBackgroundImage(UIImage(named:"Email"), for: .normal)
        viewshare.addSubview(btnemail)
        
        let lblfb=UILabel(frame: CGRect(x: 10, y:btnfb.frame.maxY, width: 60, height: 25))
        lblfb.text="Facebook"
        lblfb.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblfb.font=UIFont.systemFont(ofSize: 12)
        lblfb.textAlignment = .center
        viewshare.addSubview(lblfb)

        let lbltext=UILabel(frame: CGRect(x: viewshare.frame.size.width/2-30, y:btnfb.frame.maxY, width: 60, height: 25))
        lbltext.text="Text"
        lbltext.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lbltext.font=UIFont.systemFont(ofSize: 12)
        lbltext.textAlignment = .center
        viewshare.addSubview(lbltext)

        let lblEmail=UILabel(frame: CGRect(x: viewshare.frame.size.width-70, y:btnfb.frame.maxY, width: 60, height: 25))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblEmail.font=UIFont.systemFont(ofSize: 12)
        lblEmail.textAlignment = .center
        viewshare.addSubview(lblEmail)
        
        

    }
    
    func loadBottomCollectionview()
    {
        viewexpand=UIView(frame: CGRect(x: 0, y: appDelegate.screenHeight-150, width: appDelegate.screenWidth, height: 150))
        viewexpand.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(viewexpand)
        
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 10, right: 10)
        if appDelegate.screenWidth==320
        {
            layout.itemSize = CGSize(width: 60, height: 55)
        }
        else
        {
            layout.itemSize = CGSize(width: 75, height: 55)
        }
        expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:150), collectionViewLayout: layout)
        expandcollectionView.dataSource = self
        expandcollectionView.delegate = self
        expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        viewexpand.addSubview(expandcollectionView)
   
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selected==true
        {
            return arr_category_img1.count
        }
        else
        {
            return arr_category_img.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
        cell.backgroundColor=UIColor.clear
        let   img_category = UIImageView()
        if appDelegate.screenWidth==320
        {
            img_category.frame = CGRect(x: 17,y:0,width: 25,height:25)
        }
        else
        {
        img_category.frame = CGRect(x: 25,y:0,width: 25,height:25)
        }
        img_category.backgroundColor=UIColor.clear
        if selected==true
        {
        img_category.image=UIImage(named: arr_category_img1[indexPath.row])
        }
        else
        {
         img_category.image=UIImage(named: arr_category_img[indexPath.row])
        }
        cell.addSubview(img_category)
        
        img_category.clipsToBounds = true
        
        if appDelegate.screenWidth==320
        {
            lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:60,height:35))
        }
        else
        {
         lbl_category_name=UILabel(frame: CGRect(x:0,y:23,width:75,height:35))
        }
        if selected==true
        {
        lbl_category_name.text=arr_category_name1[indexPath.row]
        }
        else
        {
          lbl_category_name.text=arr_category_name[indexPath.row]
        }
        lbl_category_name.textColor=UIColor.black.withAlphaComponent(0.7)
        lbl_category_name.font=UIFont .systemFont(ofSize: 11)
        lbl_category_name.textAlignment = .center
        lbl_category_name.numberOfLines=2
        cell.addSubview(lbl_category_name)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if indexPath.row==3
        {
            if selected==true
            {
                selected=false
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-150, width: appDelegate.screenWidth, height: 150)
                expandcollectionView.removeFromSuperview()
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:150), collectionViewLayout: layout)
            }
            else
            {
                selected=true
                
                viewexpand.frame=CGRect(x: 0, y: appDelegate.screenHeight-210, width: appDelegate.screenWidth, height: 210)
                
                expandcollectionView.removeFromSuperview()
                
                expandcollectionView = UICollectionView(frame: CGRect(x:0,y:0,width:appDelegate.screenWidth,height:210), collectionViewLayout: layout)
            }
            
            expandcollectionView.dataSource = self
            expandcollectionView.delegate = self
            expandcollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            expandcollectionView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            viewexpand.addSubview(expandcollectionView)
        }
        else if indexPath.row==0
        {
            let propertyView = PropertySearch(nibName: "PropertySearch", bundle: nil)
            navigationController?.pushViewController(propertyView, animated: true)
        }
        else if indexPath.row==1
        {
            if appDelegate.selectuser==true
            {
                let showingView = ShowingView(nibName: "ShowingView", bundle: nil)
                navigationController?.pushViewController(showingView, animated: true)
            }
            else
            {
                let fevriteView = FavriteView(nibName: "FavriteView", bundle: nil)
                navigationController?.pushViewController(fevriteView, animated: true)
  
            }
        }
        else if indexPath.row==5
        {
            if appDelegate.selectuser==true
            {
                let contactView = ContactsView(nibName: "ContactsView", bundle: nil)
                navigationController?.pushViewController(contactView, animated: true)
            }
            else
            {
                let agentView = AgentListView(nibName: "AgentListView", bundle: nil)
                navigationController?.pushViewController(agentView, animated: true)
            }
        }
        else if indexPath.row==7
        {
            let suggestedView = SuggestedProperties(nibName: "SuggestedProperties", bundle: nil)
            navigationController?.pushViewController(suggestedView, animated: true)
        }
        else if indexPath.row==2
        {
            let massegsView = MassagesView(nibName: "MassagesView", bundle: nil)
            navigationController?.pushViewController(massegsView, animated: true)
        }
        else if indexPath.row==8
        {
            let settingView = SettingsView(nibName: "SettingsView", bundle: nil)
            navigationController?.pushViewController(settingView, animated: true)
        }
        else if indexPath.row==9
        {
            laodSharePopupUI()
        }
        else if indexPath.row==4
        {
            let calculaterView = MortgageCalculaterView(nibName: "MortgageCalculaterView", bundle: nil)
            navigationController?.pushViewController(calculaterView, animated: true)
        }

    }

    func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            // viewToAnimate.alpha = 0
        }) { _ in
            self.viewbackground.removeFromSuperview()
            self.viewshare.removeFromSuperview()
        }
    }
    
  /*  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       
            let location = locations.last! as CLLocation
        
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapview.setRegion(region, animated: true)
      }
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        defer { currentLocation = locations.last }
//        
//        if currentLocation == nil {
//            // Zoom to user location
//            if let userLocation = locations.last {
//                let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 250, 250)
//                mapview.setRegion(viewRegion, animated: false)
//            }
//        }
//    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "ic_map")
        }
        
        return annotationView
    }*/
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        let center = CLLocationCoordinate2D(latitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude))
        let span = MKCoordinateSpan(latitudeDelta:20,longitudeDelta:20)
        var region = MKCoordinateRegion(center: center, span:span)
        region.center = mapview.userLocation.coordinate
        self.mapview.setRegion(region, animated: true)
        
       let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        myAnnotation.title = "Current location"
        mapview.addAnnotation(myAnnotation)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: " identifier")
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "identifier")
            annotationView?.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView?.image = UIImage(named:"ic_map1.png")
        } else {
            annotationView?.annotation = annotation
            
        }
        return annotationView
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

