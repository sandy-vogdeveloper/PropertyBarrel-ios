//
//  contactdetailView.swift
//  PropertyBarrel
//
//  Created by Bhimashankar Vibhute on 5/13/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class contactdetailView: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate ,UITextFieldDelegate{

    var app = UIApplication.shared.delegate as! AppDelegate
    var viewbackground:UIView!
    var popview:UIView!
    
    var btnback: UIButton!
    var btncall: UIButton!
    var btnsms:  UIButton!
    var btncond: UIButton!
    var btnsold:  UIButton!
    
    var imgprofile:UIImageView!
    var imgback : UIImageView!
    var middleview : UIView!
    var bottomview : UIView!
    var imgarr = ["1","2","3"]
    
    var  mycollectionview : UICollectionView!
    var  collectionview : UICollectionView!
    var layout : UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self
            .keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        loadinitialUI()
        imagearray()
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func imagearray()
    {
        
        imgarr=["demo","demo","demo","demo","demo","demo","demo","demo","demo","demo","demo"]
    }
    
    func loadinitialUI()
    {
        let topview = UIView(frame:CGRect(x: 0, y: 0, width: app.screenWidth, height: 30))
        topview.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        self.view.addSubview(topview)
        
        let imgback1 = UIButton(frame: CGRect(x:5, y: 5, width:20, height:20))
        imgback1.setBackgroundImage(UIImage(named:"ic_det_back"), for: .normal)
        imgback1.alpha=0.6
        imgback1.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        topview.addSubview(imgback1)
        
        btnback = UIButton(frame:CGRect(x:imgback1.frame.maxX, y:5, width:50, height:20))
        btnback.setTitleColor(UIColor.init(red: 202/255.0, green: 202/255.0, blue: 202/255.0, alpha: 1.0), for: UIControlState.normal)
        btnback.setTitle("Back", for: UIControlState.normal)
        btnback.addTarget(self, action:#selector(btnclick), for: UIControlEvents.touchUpInside)
        btnback.titleLabel?.font=UIFont.systemFont(ofSize: 18)
        topview.addSubview(btnback)
        
        btncall = UIButton(frame:CGRect(x:app.screenWidth-60, y:5, width:20, height:20))
        btncall.setImage( UIImage(named: "ic_dettop_phone"), for: UIControlState.normal)
        btncall.alpha=0.6
        topview.addSubview(btncall)
        
        
        btnsms = UIButton(frame:CGRect(x:app.screenWidth-30, y:5, width:20, height:20))
        btnsms.setImage( UIImage(named: "ic_dettop_msg1"), for: UIControlState.normal)
        btnsms.titleLabel?.font=UIFont.systemFont(ofSize: 20)
        btnsms.alpha=0.6
        topview.addSubview(btnsms)
        
        imgprofile = UIImageView(frame: CGRect(x: 0, y:topview.frame.maxY, width: app.screenWidth, height:app.screenHeight/2-130))
        imgprofile.image = UIImage(named: "agent_pic")
        self.view.addSubview(imgprofile)
        
        let  lblname :UILabel=UILabel(frame: CGRect(x: 10, y: imgprofile.frame.maxY+5, width: 200, height:30))
        lblname.text = "Barbara Smith"
        lblname.textColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        lblname.font = UIFont.boldSystemFont(ofSize: 17)
        lblname.textAlignment = .left
        self.view.addSubview(lblname)
        
        let  lblphone :UILabel=UILabel(frame: CGRect(x: 10, y: lblname.frame.maxY, width: 200, height: 20))
        lblphone.text = "Phone:"
        lblphone.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblphone.font = UIFont.systemFont(ofSize: 14)
        lblphone.textAlignment = .left
        self.view.addSubview(lblphone)
        
        let  lblno :UILabel=UILabel(frame: CGRect(x: 10, y: lblphone.frame.maxY, width: 200, height: 20))
        lblno.text = "1-403-555-4444"
        lblno.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblno.font = UIFont.systemFont(ofSize: 14)
        lblno.textAlignment = .left
        self.view.addSubview(lblno)
        
        btncond  = UIButton(type: UIButtonType.roundedRect) as UIButton
        btncond.layer.cornerRadius = 7.0
        btncond.frame=CGRect(x:app.screenWidth-160, y:lblname.frame.maxY, width:140, height:30)
        btncond.setTitle("Conditional Sold", for: UIControlState.normal)
        btncond.setTitleColor(UIColor.white, for: .normal)
        btncond.backgroundColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        btncond.titleLabel?.font=UIFont.systemFont(ofSize:14)
        btncond.addTarget(self, action:#selector(btnsoldclick), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btncond)
        
        let  lblemail :UILabel=UILabel(frame: CGRect(x: 10, y: lblno.frame.maxY, width: 200, height: 20))
        lblemail.text = "Email:"
        lblemail.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblemail.font = UIFont.boldSystemFont(ofSize: 15)
        lblemail.textAlignment = .left
        self.view.addSubview(lblemail)
        
        let  lblemail1 :UILabel=UILabel(frame: CGRect(x: 10, y:lblemail.frame.maxY, width: 200, height:18))
        lblemail1.text = "babs@email.com"
        lblemail1.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblemail1.font = UIFont.systemFont(ofSize: 15)
        lblemail1.textAlignment = .left
        self.view.addSubview(lblemail1)
        
        btnsold = UIButton(type: UIButtonType.roundedRect) as UIButton
        btnsold.layer.cornerRadius = 7.0
        btnsold.frame = CGRect(x:app.screenWidth-160, y:btncond.frame.maxY+10, width:140, height:30)
        btnsold.setTitle("Sold", for: UIControlState.normal)
        btnsold.setTitleColor(UIColor.white, for: .normal)
        btnsold.backgroundColor = UIColor.init(colorLiteralRed: 2.0/255.0, green: 82.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        btnsold.titleLabel?.font=UIFont.systemFont(ofSize: 14)
        btnsold.addTarget(self, action:#selector(btnsoldclick), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btnsold)
        
        middleview = UIView(frame: CGRect(x: 0, y:lblemail1.frame.maxY, width: app.screenWidth, height: 30))
        middleview.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        self.view.addSubview(middleview)
        
        let  lblpro :UILabel=UILabel(frame: CGRect(x: 10, y:2, width:app.screenWidth, height:25))
        lblpro.text = "Suggested Properties"
        lblpro.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblpro.font = UIFont.systemFont(ofSize: 14)
        lblpro.textAlignment = .left
        middleview.addSubview(lblpro)
        
        layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left:0, bottom:0, right:0)
        layout.itemSize = CGSize(width:108, height:80)
        
        mycollectionview = UICollectionView(frame: CGRect(x:0,y:middleview.frame.maxY,
                                                          width:app.screenWidth, height:85), collectionViewLayout: layout)
        mycollectionview.collectionViewLayout = layout
        mycollectionview.isScrollEnabled = true
        mycollectionview.showsHorizontalScrollIndicator = true
        mycollectionview.isPagingEnabled = true
        mycollectionview.delegate = self
        mycollectionview.dataSource = self
        mycollectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "mycell")
        mycollectionview.backgroundColor = .white
        self.view.addSubview(mycollectionview)
        
        bottomview = UIView(frame: CGRect(x: 0, y:mycollectionview.frame.maxY, width: app.screenWidth, height: 25))
        bottomview.backgroundColor = UIColor.init(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
        self.view.addSubview(bottomview)
        
        let  lblfav :UILabel=UILabel(frame: CGRect(x: 10, y:2, width:app.screenWidth, height:20))
        lblfav.text = "Favourited Properties"
        lblfav.textColor = UIColor.init(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        lblfav.font = UIFont.systemFont(ofSize:14)
        lblfav.textAlignment = .left
        bottomview.addSubview(lblfav)
        
        collectionview = UICollectionView(frame: CGRect(x:0,y:bottomview.frame.maxY,
                                                        width:app.screenWidth, height:80), collectionViewLayout: layout)
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.collectionViewLayout = layout
        collectionview.showsHorizontalScrollIndicator = true
        collectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "mycell")
        collectionview.backgroundColor = .white
        self.view.addSubview(collectionview)
        
    }
    
    func loadSpecialInfo()
    {
        viewbackground=UIView(frame: CGRect(x: 0, y: 0, width: app.screenWidth, height: app.screenHeight))
        viewbackground.backgroundColor=UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewbackground)
        
        popview = UIView(frame: CGRect(x:15, y:80, width:app.screenWidth-30, height:350))
        popview.backgroundColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        popview.layer.cornerRadius = 15.0
        self.view.addSubview(popview)
        
        let lblbooking = UILabel(frame: CGRect(x:5, y:5, width:popview.frame.size.width, height: 25))
        lblbooking.text = "Special Info"
        lblbooking.font = UIFont.systemFont(ofSize:18)
        lblbooking.textAlignment = .center
        lblbooking.textColor = UIColor.init(red: 78/255.0, green: 118/255.0, blue: 143/255.0, alpha: 1.0)
        popview.addSubview(lblbooking)
        
        let lblclose = UILabel(frame: CGRect(x:10, y:lblbooking.frame.maxY, width:150, height:20))
        lblclose.text = "Close Date:"
        lblclose.font = UIFont.boldSystemFont(ofSize: 15)
        lblclose.textAlignment = .left
        lblclose.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lblclose)
        
        
      let  txtclose = UITextField(frame: CGRect(x:10,y:lblclose.frame.maxY,width:popview.frame.size.width-20,height:30))
        txtclose.backgroundColor = UIColor.white
        txtclose.layer.cornerRadius = 5.0
        txtclose.layer.borderWidth=1.5
        txtclose.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtclose.borderStyle = UITextBorderStyle.roundedRect
        txtclose.delegate=self
        popview.addSubview(txtclose)
        
        let lbldown = UILabel(frame: CGRect(x:10, y: txtclose.frame.maxY, width:150, height: 20))
        lbldown.text = "Possesion Date:"
        lbldown.font = UIFont.boldSystemFont(ofSize: 15)
        lbldown.textAlignment = .left
        lbldown.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lbldown)
        
      let  txtposs = UITextField(frame: CGRect(x:10,y:lbldown.frame.maxY,width:popview.frame.size.width-20,height:30))
        txtposs.layer.cornerRadius = 5.0
        txtposs.layer.borderWidth=1.5
        txtposs.borderStyle = UITextBorderStyle.roundedRect
        txtposs.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtposs.delegate=self
        txtposs.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        popview.addSubview(txtposs)
        
        
        let lblinterest = UILabel(frame: CGRect(x: 10, y:txtposs.frame.maxY, width:150, height: 20))
        lblinterest.text = "Mortage Broker:"
        lblinterest.font = UIFont.boldSystemFont(ofSize:15)
        lblinterest.textAlignment = .left
        lblinterest.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lblinterest)
        
      let  txtmortage = UITextField(frame: CGRect(x:10,y: lblinterest.frame.maxY,width:popview.frame.size.width-20,height:30))
        txtmortage.backgroundColor = UIColor.white
        txtmortage.layer.borderWidth=1.5
        txtmortage.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtmortage.borderStyle = UITextBorderStyle.roundedRect
        txtmortage.layer.cornerRadius = 5.0
        txtmortage.delegate=self
        txtmortage.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        popview.addSubview(txtmortage)
        
        let lblamort = UILabel(frame: CGRect(x:10, y:txtmortage.frame.maxY, width:150, height: 20))
        lblamort.text = "Home Inspector:"
        lblamort.font = UIFont.boldSystemFont(ofSize: 15)
        lblamort.textAlignment = .left
        lblamort.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lblamort)
        
      let  txthome = UITextField(frame: CGRect(x:10,y:lblamort.frame.maxY,width:popview.frame.size.width-20,height:30))
        txthome.delegate=self
        txthome.layer.borderWidth=1.5
        txthome.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txthome.layer.cornerRadius = 5.0
        txthome.borderStyle = UITextBorderStyle.roundedRect
        txthome.textColor = UIColor.init(red: 43/255.0, green: 82/255.0, blue: 117/255.0, alpha: 1.0)
        popview.addSubview(txthome)
        
        let lblfreq = UILabel(frame: CGRect(x:10, y: txthome.frame.maxY, width:150, height: 20))
        lblfreq.text = "Lawyer:"
        lblfreq.font = UIFont.boldSystemFont(ofSize: 15)
        lblfreq.textAlignment = .left
        lblfreq.textColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lblfreq)
        
      let  txtlawyer = UITextField(frame: CGRect(x:10,y:lblfreq.frame.maxY,width:popview.frame.size.width-20,height:30))
        txtlawyer.backgroundColor = UIColor.white
        txtlawyer.layer.cornerRadius = 5.0
        txtlawyer.layer.borderWidth=1.5
        txtlawyer.layer.borderColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0).cgColor
        txtlawyer.borderStyle = UITextBorderStyle.roundedRect
        txtlawyer.delegate=self
        popview.addSubview(txtlawyer)
        
        let middleview = UIView(frame: CGRect(x:0, y:popview.frame.size.height-36,width:popview.frame.size.width, height:0.6))
        middleview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(middleview)
        
        
       let btncancle = UIButton(frame:CGRect(x:5, y:middleview.frame.maxY, width:popview.frame.size.width/2-20, height:35))
        btncancle.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:137/255.0, alpha: 1.0), for: UIControlState.normal)
        btncancle.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btncancle.addTarget(self, action:#selector(applybtnclick), for: UIControlEvents.touchUpInside)
        btncancle.setTitle("Cancle", for: UIControlState.normal)
        popview.addSubview(btncancle)
        
        
      let  btnApply = UIButton(frame:CGRect(x:btncancle.frame.maxX+25 , y:middleview.frame.maxY, width:popview.frame.size.width/2-10, height:35))
        btnApply.setTitleColor(UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0), for: UIControlState.normal)
        btnApply.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btnApply.addTarget(self, action:#selector(applybtnclick), for: UIControlEvents.touchUpInside)
        btnApply.setTitle("Apply", for: UIControlState.normal)
        popview.addSubview(btnApply)
        
        let lineview = UIView(frame: CGRect(x:btncancle.frame.maxX+8, y:popview.frame.size.height-36,width:0.6, height:36))
        lineview.backgroundColor = UIColor.init(red:136/255.0, green: 136/255.0, blue:
            137/255.0, alpha: 1.0)
        popview.addSubview(lineview)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgarr.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let mycell = collectionView.dequeueReusableCell(withReuseIdentifier: "mycell", for: indexPath)
        
        let imgview=UIImageView()
        imgview.frame  = CGRect(x:5, y:0, width:110, height:75)
        imgview.image  = UIImage(named:imgarr[indexPath.row])
        mycell.contentView.addSubview(imgview)
        
        return mycell
    }
   
    
    func btnclick()
    {
      _ = navigationController?.popViewController(animated: true)
    }
    
    func btnsoldclick()
    {
        loadSpecialInfo()
    }

    func applybtnclick()
    {
        viewbackground.removeFromSuperview()
        popview.removeFromSuperview()
    }
    
    //UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    //Keyboard Show & Hide methods
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -100
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
